package pt.ul.armus.edgebuffer.redis;

import java.util.Collection;

import pt.ul.armus.edgebuffer.EdgeSet;

/**
 * A Data Access Object for the state graph representation that Armus uses
 * internally.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public interface ArmusDAO {
	/**
	 * Marks a task as blocked.
	 * @param taskKey
	 * @param group
	 */
	void setBlocked(String taskKey, EdgeSet group);

	/**
	 * Returns the contents of the task.  
	 * @param id
	 * @return Must return null if the class is unblocked.
	 */
	EdgeSet getBlocked(String id);
	
	/**
	 * Tests whether a task is blocked.
	 * @param taskKey
	 * @return
	 */
	boolean isBlocked(String taskKey);

	/**
	 * Marks the task as unblocked.
	 * @param taskKey
	 */
	void clearBlocked(String taskKey);

	/**
	 * Removes all data associated with this task.
	 * @param taskKey
	 */
	void removeTask(String taskKey);

	/**
	 * Removes all entries that are currently handled.
	 */
	void removeAll();

	/**
	 * Creates a new task.
	 * @return
	 */
	String createTask();

	/**
	 * Gets the content of all tasks.
	 * @return
	 */
	Collection<EdgeSet> getAllBlocked();
}