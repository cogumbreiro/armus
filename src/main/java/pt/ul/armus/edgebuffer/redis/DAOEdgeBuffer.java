package pt.ul.armus.edgebuffer.redis;

import static pt.ul.armus.util.IterableUtils.zip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

import pt.ul.armus.edgebuffer.EdgeBuffer;
import pt.ul.armus.edgebuffer.EdgeSet;
import pt.ul.armus.edgebuffer.TaskHandle;

/**
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class DAOEdgeBuffer implements EdgeBuffer {
	private final Map<TaskHandle, String> handles = new ConcurrentHashMap<>();
	private final Collection<String> toDelete = new ConcurrentLinkedDeque<>();
	private final EdgeBuffer buffer;
	private final ArmusDAO dao;

	public DAOEdgeBuffer(EdgeBuffer buffer, ArmusDAO dao) {
		this.buffer = buffer;
		this.dao = dao;
	}

	@Override
	public TaskHandle createHandle() {
		return buffer.createHandle();
	}

	@Override
	public void clear() {
		buffer.clear();
		dao.removeAll(); // this is a racy operation
	}

	@Override
	public void destroy(TaskHandle handle) {
		buffer.destroy(handle);
		String key = handles.remove(handle);
		if (key != null) {
			toDelete.add(key);
		}
	}

	@Override
	public Collection<EdgeSet> getBlockedEdges() {
		return buffer.getBlockedEdges();
	}

	@Override
	public Collection<TaskHandle> getHandles() {
		return buffer.getHandles();
	}

	@Override
	public Map<TaskHandle, EdgeSet> getAll() {
		return buffer.getAll();
	}

	/**
	 * Write changes to the DAO.
	 */
	public void writeToDAO() {
		Map<String, EdgeSet> updates = new HashMap<>();
		List<TaskHandle> newHandles = new ArrayList<>();
		List<EdgeSet> inserts = new ArrayList<>();
		for (Entry<TaskHandle, EdgeSet> entry : buffer.getAll().entrySet()) {
			String daoHandle = handles.get(entry.getKey());
			if (daoHandle != null) {
				newHandles.add(entry.getKey());
				inserts.add(entry.getValue());
			} else {
				updates.put(daoHandle, entry.getValue());
			}
		}
		List<String> removes = new ArrayList<>(toDelete);
		toDelete.removeAll(removes);
		List<String> newIds = executeBatch(inserts, updates, removes);
		for (Entry<TaskHandle, String> entry : zip(newHandles, newIds)) {
			handles.put(entry.getKey(), entry.getValue());
		}
	}

	/**
	 * Keep it simple.
	 * @param inserts
	 * @param updates
	 * @param removes
	 * @return
	 */
	private List<String> executeBatch(List<EdgeSet> inserts,
			Map<String, EdgeSet> updates, List<String> removes) {
		// add + set
		List<String> newKeys = new ArrayList<>();
		for (EdgeSet edge : inserts) {
			String taskId = dao.createTask();
			dao.setBlocked(taskId, edge);
			newKeys.add(taskId);
		}
		// update
		for (Entry<String, EdgeSet> entry : updates.entrySet()) {
			dao.setBlocked(entry.getKey(), entry.getValue());
		}
		// remove
		for (String toRemove : removes) {
			dao.removeTask(toRemove);
		}
		return newKeys;
	}

}
