package pt.ul.armus.edgebuffer.redis;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ul.armus.edgebuffer.EdgeBuffer;
import pt.ul.armus.edgebuffer.EdgeBufferFactory;
import pt.ul.armus.edgebuffer.HashEdgeBufferFactory;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Creates an edge buffer backed by Redis.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class RedisEdgeBufferFactory implements EdgeBufferFactory {
	public static final String HOST_SYSTEM_PROPERTY_KEY = "armus.redis.host";
	public static final String KEY_PREFIX_SYSTEM_PROPERTY_KEY = "armus.redis.keyPrefix";
	private static final Logger LOG = Logger.getLogger(RedisEdgeBufferFactory.class.getName());
	
	private static String getProp(String key, String defaultValue) {
		final String value = System.getProperty(key, defaultValue);
		LOG.log(Level.CONFIG, String.format("Loading system property %s = '%s' (default: '%s')", key, value, defaultValue));
		return value;
	}
	
	@Override
	public EdgeBuffer createEdgeBuffer() {
		// XXX: note that we do not use the task handle factory
		// XXX: there is no support for deadlock avoidance
		String host = getProp(HOST_SYSTEM_PROPERTY_KEY, "localhost");
		String prefix = getProp(KEY_PREFIX_SYSTEM_PROPERTY_KEY, "armus");
		JedisPoolConfig conf = new JedisPoolConfig();
		conf.setBlockWhenExhausted(false);
		int numberOfCores = Runtime.getRuntime().availableProcessors();
		conf.setMaxTotal(numberOfCores * 4);
        conf.setMaxIdle(numberOfCores * 4);
		JedisPool jedis = new JedisPool(conf, host);
		JedisDAO dao = new JedisDAO(jedis, ResourceConverter.DEFAULT, new KeyGenerator(prefix));
		HashEdgeBufferFactory bufferFactory = new HashEdgeBufferFactory();
		EdgeBuffer buffer = bufferFactory.createEdgeBuffer();
		final DAOEdgeBuffer result = new DAOEdgeBuffer(buffer, dao);
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				result.writeToDAO();
			}
		};
		Timer timer = new Timer(true);
		timer.schedule(task, 200, 200);
		return result;
	}

}
