package pt.ul.armus.edgebuffer.redis;

import redis.clients.jedis.JedisPool;

public class Pool {
	private final JedisPool pool;
	
	public Pool(JedisPool pool) {
		this.pool = pool;
	}

	public PoolEntry getResource() {
		return new PoolEntry(pool, pool.getResource());
	}
}
