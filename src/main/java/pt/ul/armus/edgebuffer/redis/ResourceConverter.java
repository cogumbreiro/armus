package pt.ul.armus.edgebuffer.redis;

import pt.ul.armus.Resource;

/**
 * Converts resources to strings.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class ResourceConverter implements Converter<Resource,String> {

	@Override
	public Resource fromData(String str) {
		return new RedisResource(str);
	}
	
	@Override
	public String toData(Resource res) {
		if (res instanceof RedisResource) {
			return ((RedisResource) res).getId();
		}
		return res.toString(); 
	}
	
	/**
	 * The default converter.
	 */
	public static final ResourceConverter DEFAULT = new ResourceConverter();
}
