package pt.ul.armus.edgebuffer.redis;

public interface Converter<T,D> {
	T fromData(D value);
	D toData(T stringRep);
}
