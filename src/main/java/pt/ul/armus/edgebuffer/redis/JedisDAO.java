package pt.ul.armus.edgebuffer.redis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import pt.ul.armus.Resource;
import pt.ul.armus.edgebuffer.EdgeSet;
import pt.ul.armus.edgebuffer.EdgeSetFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import redis.clients.jedis.Transaction;

/**
 * Implements the DAO using a Redis data store, with the Jedis bindings.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class JedisDAO implements ArmusDAO {
	private static final int MAX_TRIES = 100;

	private final class Entry {
		private final Response<List<String>> successors;
		private final Response<List<String>> predecessors;

		public Entry(Response<List<String>> successors,
				Response<List<String>> predecessors) {
			this.successors = successors;
			this.predecessors = predecessors;
		}

		public List<String> getSuccessors() {
			return successors.get();
		}

		public List<String> getPredecessors() {
			return predecessors.get();
		}

		public EdgeSet build() {
			List<String> succAsStr = getSuccessors();
			List<String> predAsStr = getPredecessors();
			if (succAsStr.isEmpty() || predAsStr.isEmpty()) {
				return null;
			}
			List<Resource> succ = fromData(succAsStr, resourceConverter);
			List<Resource> pred = fromData(predAsStr, resourceConverter);
			return EdgeSetFactory.requestManyAllocateMany(succ, pred);
		}
	}

	private static List<Resource> fromData(List<String> res,
			Converter<Resource, String> conv) {
		ArrayList<Resource> result = new ArrayList<>(res.size());
		for (String data : res) {
			result.add(conv.fromData(data));
		}
		return result;
	}

	private final Pool pool;
	private final Converter<Resource, String> resourceConverter;
	private final KeyGenerator keyGen;

	/**
	 * The default constructor.
	 * 
	 * @param pool
	 * @param resourceConverter
	 * @param keyGen
	 */
	public JedisDAO(JedisPool pool,
			Converter<Resource, String> resourceConverter, KeyGenerator keyGen) {
		this.pool = new Pool(pool);
		this.resourceConverter = resourceConverter;
		this.keyGen = keyGen;
	}

	@Override
	public void setBlocked(String taskKey, EdgeSet group) {
		if (group == null) {
			clearBlocked(taskKey);
			return;
		}
		try (PoolEntry entry = pool.getResource()) {
			Jedis client = entry.getJedis();
			Transaction tx = client.multi();
			setBlocked(tx, taskKey, group);
			tx.exec();
		}
	}
	
	private void setBlocked(Transaction tx, String taskKey, EdgeSet group) {
		setListOfResources(tx, getPredecessors(taskKey),
				group.getAllocated());
		setListOfResources(tx, getSuccessors(taskKey),
				group.getRequested());
	}

	@Override
	public boolean isBlocked(String taskKey) {
		try (PoolEntry entry = pool.getResource()) {
			Jedis client = entry.getJedis();
			return client.exists(getPredecessors(taskKey))
					&& client.exists(getSuccessors(taskKey));
		}
	}

	@Override
	public void clearBlocked(String taskKey) {
		try (PoolEntry entry = pool.getResource()) {
			Jedis client = entry.getJedis();
			Pipeline pipe = client.pipelined();
			pipe.del(getPredecessors(taskKey));
			pipe.del(getSuccessors(taskKey));
			pipe.sync();
		}
	}

	/**
	 * Replaces a key with a list of resources.
	 * 
	 * @param client
	 * @param key
	 * @param resources
	 */
	private void setListOfResources(Transaction tx, final String key,
			final Iterable<Resource> resources) {
		tx.del(key);
		ArrayList<String> resourcesAsStr = new ArrayList<>();
		for (Resource res : resources) {
			resourcesAsStr.add(resourceConverter.toData(res));
		}
		tx.lpush(key,
				resourcesAsStr.toArray(new String[resourcesAsStr.size()]));
	}

	@Override
	public void removeTask(String taskKey) {
		try (PoolEntry entry = pool.getResource()) {
			Jedis client = entry.getJedis();
			Pipeline pipe = client.pipelined();
			removeTask(pipe, taskKey);
			pipe.sync();
		}
	}

	private void removeTask(Pipeline pipe, String taskKey) {
		pipe.del(getPredecessors(taskKey));
		pipe.del(getSuccessors(taskKey));
		pipe.srem(keyGen.getTasks(), taskKey);
	}

	private String getSuccessors(String taskKey) {
		return keyGen.getSuccessors(taskKey);
	}

	private String getPredecessors(String taskKey) {
		return keyGen.getPredecessors(taskKey);
	}

	@Override
	public void removeAll() {
		try (PoolEntry entry = pool.getResource()) {
			Jedis client = entry.getJedis();
			Set<String> tasks = getAllTasks(client);
			final Pipeline pipe = client.pipelined();
			pipe.del(keyGen.getLastTaskId());
			pipe.del(keyGen.getTasks());
			for (String key : tasks) {
				removeTask(pipe, key);
			}
			pipe.sync();
		}
	}

	@Override
	public String createTask() {
		try (PoolEntry entry = pool.getResource()) {
			Jedis client = entry.getJedis();
			String taskKey = null;
			int tries = MAX_TRIES;
			do {
				taskKey = keyGen.getTaskId(client.incr(keyGen.getLastTaskId()));
				taskKey = client.sadd(keyGen.getTasks(), taskKey).intValue() != 0 ? taskKey
						: null;
				tries--;
			} while (taskKey == null && tries > 0);
			return taskKey;
		}
	}
	
	@Override
	public Collection<EdgeSet> getAllBlocked() {
		try (PoolEntry poolEntry = pool.getResource()) {
			Jedis client = poolEntry.getJedis();
			Set<String> tasks = getAllTasks(client);
			final Pipeline pipe = client.pipelined();
			// get all edges
			ArrayList<Entry> results = new ArrayList<>();
			for (String key : tasks) {
				results.add(getBlocked(pipe, key));
			}
			pipe.sync();
			// now build all edges
			ArrayList<EdgeSet> result = new ArrayList<>(results.size());
			for (Entry entry : results) {
				EdgeSet edges = entry.build();
				// skip the ones that are null (i.e., are unset or are empty)
				if (edges != null) {
					result.add(edges);
				}
			}
			return result;
		}
	}

	private Entry getBlocked(final Pipeline pipe, String key) {
		Response<List<String>> prec = pipe.lrange(getPredecessors(key), 0, -1);
		Response<List<String>> succ = pipe.lrange(getSuccessors(key), 0, -1);
		return new Entry(succ, prec);
	}

	private Set<String> getAllTasks(Jedis client) {
		Set<String> tasks = client.smembers(keyGen.getTasks());
		return tasks;
	}

	@Override
	public EdgeSet getBlocked(String id) {
		try (PoolEntry poolEntry = pool.getResource()) {
			Jedis client = poolEntry.getJedis();
			Pipeline pipe = client.pipelined();
			Entry entry = getBlocked(pipe, id);
			pipe.sync();
			return entry.build();
		}
	}
}
