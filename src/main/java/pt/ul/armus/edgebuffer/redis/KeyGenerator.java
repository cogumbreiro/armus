package pt.ul.armus.edgebuffer.redis;

import pt.ul.armus.util.StringUtils;

/**
 * Generates the Redis keys used to store the edges. The description of each key
 * is described in the methods of this class.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class KeyGenerator {

	private static final String SEP = ":";
	private final String prefix;
	private final String tasks;
	private final String lastTaskId;
	
	private static String key(String...parts) {
		return StringUtils.join(SEP, parts);
	}

	public KeyGenerator(String prefix) {
		this.prefix = prefix;
		this.tasks = key(prefix, "tasks");
		this.lastTaskId = key(prefix, "last_task_id");
	}

	/**
	 * A redis set. Format: "PREFIX:tasks"
	 * 
	 * @return
	 */
	public String getTasks() {
		return tasks;
	}

	/**
	 * The prefix for task keys. Format: "PREFIX:task:ID"
	 * 
	 * @param id
	 * @return
	 */
	public String getTaskId(final Long id) {
		return key(prefix, "task", id.toString());
	}

	/**
	 * A redis list. The list of predecessors. Format:
	 * "TASKPREFIX:predecessors".
	 * 
	 * @param task
	 * @return
	 */
	public String getPredecessors(final String task) {
		return key(task, "predecessors");
	}

	/**
	 * A redis list. The list of successors. Format: "TASKPREFIX:predecessors".
	 * 
	 * @param task
	 * @return
	 */
	public String getSuccessors(final String task) {
		return key(task, "successors");
	}

	/**
	 * A redis integer. The last task id counter, see
	 * {@link KeyGenerator#getTaskId(Long)}. Format: "PREFIX:last_task_id"
	 * 
	 * @return
	 */
	public String getLastTaskId() {
		return lastTaskId;
	}
}
