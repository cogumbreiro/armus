package pt.ul.armus.edgebuffer.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class PoolEntry implements AutoCloseable {
	private final JedisPool pool;
	private final Jedis jedis;
	
	public PoolEntry(JedisPool pool, Jedis jedis) {
		this.pool = pool;
		this.jedis = jedis;
	}

	@Override
	public void close() {
		pool.returnResourceObject(jedis);
	}
	
	public Jedis getJedis() {
		return jedis;
	}
}
