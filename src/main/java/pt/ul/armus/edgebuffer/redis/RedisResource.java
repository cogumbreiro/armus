package pt.ul.armus.edgebuffer.redis;

import pt.ul.armus.Resource;

/**
 * This is a redis resource.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class RedisResource implements Resource {

	private final String id;
	
	public RedisResource(String id) {
		this.id = id;
	}

	/**
	 * Returns the unique identifier of this resource.
	 * @return
	 */
	public String getId() {
		return id;
	}
	
	@Override
	public Object getSynch() {
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RedisResource other = (RedisResource) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RedisResource [id=" + id + "]";
	}
}
