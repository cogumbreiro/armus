package pt.ul.armus.edgebuffer;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Using a {@link ConcurrentHashMap} as a backend for storing the
 * {@link TaskHandle}.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class HashEdgeBufferFactory implements EdgeBufferFactory {
	@Override
	public EdgeBuffer createEdgeBuffer() {
		return new HashEdgeBuffer(DefaultTaskHandleFactory.DEFAULT);
	}
}
