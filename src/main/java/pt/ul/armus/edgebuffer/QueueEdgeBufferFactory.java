package pt.ul.armus.edgebuffer;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * An edge buffer that uses a {@link ConcurrentLinkedQueue} to store the edge
 * sets.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class QueueEdgeBufferFactory implements EdgeBufferFactory {

	@Override
	public EdgeBuffer createEdgeBuffer() {
		return new QueueEdgeBuffer(DefaultTaskHandleFactory.DEFAULT);
	}
}
