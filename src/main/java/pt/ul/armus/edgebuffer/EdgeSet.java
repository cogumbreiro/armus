package pt.ul.armus.edgebuffer;

import java.util.Collection;

import pt.ul.armus.Edge;
import pt.ul.armus.Resource;

/**
 * Represents a collection of constraints.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@di.fc.ul.pt)
 * 
 */
public interface EdgeSet extends Iterable<Edge<Resource>> {
	/**
	 * Get the predecessors of an edge set.
	 * 
	 * @return
	 */
	Collection<Resource> getAllocated();

	/**
	 * Get the successors of an edge set.
	 * 
	 * @return
	 */
	Collection<Resource> getRequested();
}