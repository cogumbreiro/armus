package pt.ul.armus.edgebuffer;

/**
 * Creates {@link EdgeBuffer}s.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public interface EdgeBufferFactory {
	EdgeBuffer createEdgeBuffer();
}
