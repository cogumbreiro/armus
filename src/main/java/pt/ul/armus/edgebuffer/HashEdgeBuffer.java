package pt.ul.armus.edgebuffer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Using a {@link ConcurrentHashMap} as a backend for storing the {@link TaskHandle}.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
class HashEdgeBuffer implements EdgeBuffer {
	private final ConcurrentMap<TaskHandle, TaskHandle> sharedEdges = new ConcurrentHashMap<>();
	private final TaskHandleFactory taskHandleFactory;

	public HashEdgeBuffer(TaskHandleFactory taskHandleFactory) {
		this.taskHandleFactory = taskHandleFactory;
	}
	
	@Override
	public TaskHandle createHandle() {
		TaskHandle handle = taskHandleFactory.createTaskHandle();
		sharedEdges.put(handle, handle);
		return handle;
	}

	@Override
	public void clear() {
		sharedEdges.clear();
	}

	@Override
	public void destroy(TaskHandle handle) {
		if (handle.isBlocked()) {
			throw new IllegalStateException("Call clearBlocked() first.");
		}
		sharedEdges.remove(handle);
	}

	@Override
	public Collection<TaskHandle> getHandles() {
		return sharedEdges.keySet();
	}

	@Override
	public Collection<EdgeSet> getBlockedEdges() {
		Collection<EdgeSet> result = new ArrayList<>();
		for (TaskHandle handle : sharedEdges.keySet()) {
			EdgeSet edges = handle.getBlocked();
			if (edges != null) {
				result.add(edges);
			}
		}
		return result;
	}

	@Override
	public Map<TaskHandle, EdgeSet> getAll() {
		Map<TaskHandle,EdgeSet> result = new HashMap<>();
		for (TaskHandle handle : sharedEdges.keySet()) {
			result.put(handle, handle.getBlocked());
		}
		return result;
	}

}
