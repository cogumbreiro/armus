package pt.ul.armus.edgebuffer;

import pt.ul.armus.deadlockresolver.DeadlockFoundException;

/**
 * A {@link TaskHandle} is used by a task to control the verification strategy.
 * 
 * @author Tiago Cogumbreiro
 */
public interface TaskHandle {
	/**
	 * This method should only be invoked by the owner of this task-handle (the
	 * thread that writes the blocked status). Otherwise, the issuer thread
	 * might observe a stale value.
	 * 
	 * For inter-thread communication use {@link TaskHandle#getBlocked()}.
	 * 
	 * @return Tests if this task is marked as blocked, which happens if the
	 *         {@link TaskHandle#getBlocked()} is <code>null</code>.
	 */
	boolean isBlocked();

	/**
	 * Marks the given task as blocked. When the exception is thrown, the user
	 * must check if the task is still blocked and handle that state
	 * accordingly.
	 * 
	 * @param handle
	 *            The task.
	 * @param group
	 *            The resources in which the task is blocked.
	 * @throws when
	 *             a deadlock is found
	 */
	void setBlocked(EdgeSet group) throws DeadlockFoundException;

	/**
	 * Obtains the edge set associated with this task handle. It should return
	 * null only if the task is unblocked (isBlocked() == false).
	 * 
	 * @return null iff unset.
	 */
	EdgeSet getBlocked();

	/**
	 * Marks the given task as unblocked.
	 * 
	 * @param handle
	 */
	void clearBlocked();
}
