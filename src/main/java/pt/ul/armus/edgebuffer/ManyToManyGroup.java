package pt.ul.armus.edgebuffer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import pt.ul.armus.Edge;
import pt.ul.armus.Resource;

/**
 * Creates a restriction where in the right-hand side is a collection.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 * @param <V>
 */
class ManyToManyGroup implements EdgeSet {
	private final Resource[] requested;
	private final Resource[] allocated;

	/**
	 * leftCollection &lt; right
	 * 
	 * @param allocated
	 * @param left
	 */
	public ManyToManyGroup(Resource[] requested, Resource[] allocated) {
		this.requested = requested;
		this.allocated = allocated;
	}

	@Override
	public String toString() {
		return "ManyToManyPrecedes(requested=" + Arrays.toString(requested) + ", allocated="
				+ Arrays.toString(allocated) + ")";
	}

	@Override
	public Iterator<Edge<Resource>> iterator() {
		ArrayList<Edge<Resource>> result = new ArrayList<>(allocated.length);
		for (Resource req : requested) {
			for (Resource alloc : allocated) {
				result.add(new Edge<>(req, alloc));
			}
		}
		return result.iterator();
	}

	@Override
	public Collection<Resource> getAllocated() {
		return Arrays.asList(allocated);
	}

	@Override
	public Collection<Resource> getRequested() {
		return Arrays.asList(requested);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(allocated);
		result = prime * result + Arrays.hashCode(requested);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ManyToManyGroup other = (ManyToManyGroup) obj;
		if (!Arrays.equals(allocated, other.allocated))
			return false;
		if (!Arrays.equals(requested, other.requested))
			return false;
		return true;
	}
}
