package pt.ul.armus.edgebuffer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * An edge buffer that uses a {@link ConcurrentLinkedQueue} to store the edge
 * sets.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
class QueueEdgeBuffer implements EdgeBuffer {
	private final Queue<TaskHandle> sharedEdges = new ConcurrentLinkedQueue<>();
	private final TaskHandleFactory taskHandleFactory;

	public QueueEdgeBuffer(TaskHandleFactory taskHandleFactory) {
		this.taskHandleFactory = taskHandleFactory;
	}

	@Override
	public TaskHandle createHandle() {
		TaskHandle handle = taskHandleFactory.createTaskHandle();
		sharedEdges.add(handle);
		return handle;
	}

	@Override
	public void clear() {
		sharedEdges.clear();
	}

	@Override
	public void destroy(TaskHandle handle) {
		if (handle.isBlocked()) {
			throw new IllegalStateException("Call clearBlocked() first.");
		}
		sharedEdges.remove(handle);
	}

	@Override
	public Collection<TaskHandle> getHandles() {
		return Collections.unmodifiableCollection(sharedEdges);
	}

	@Override
	public Collection<EdgeSet> getBlockedEdges() {
		Collection<EdgeSet> result = new ArrayList<>();
		for (TaskHandle handle : sharedEdges) {
			EdgeSet edges = handle.getBlocked();
			if (edges != null) {
				result.add(edges);
			}
		}
		return result;
	}

	@Override
	public Map<TaskHandle, EdgeSet> getAll() {
		Map<TaskHandle, EdgeSet> edges = new HashMap<TaskHandle, EdgeSet>();
		for (TaskHandle handle : sharedEdges) {
			edges.put(handle, handle.getBlocked());
		}
		return edges;
	}
}
