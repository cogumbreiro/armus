package pt.ul.armus.edgebuffer;

import java.util.Collection;
import java.util.Map;

/**
 * Represents the edge buffer. It stores instances of {@link EdgeSet} that can
 * be iterated over. Each {@link EdgeSet} is maintained by an instance of a
 * {@link TaskHandle}.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public interface EdgeBuffer {
	/**
	 * Create a new task handler.
	 * 
	 * @return
	 */
	TaskHandle createHandle();

	/**
	 * Remove all edge sets.
	 */
	void clear();

	/**
	 * Reclaim the task handle. To be used when the task terminates.
	 * 
	 * @param handle
	 */
	void destroy(TaskHandle handle);
	
	/**
	 * Get the blocked edges.
	 * @return
	 */
	Collection<EdgeSet> getBlockedEdges();
	
	/**
	 * Get all available task handles.
	 * @return
	 */
	Collection<TaskHandle> getHandles();

	/**
	 * Get a snapshot of the edge buffer.
	 * @return
	 */
	Map<TaskHandle, EdgeSet> getAll();
}
