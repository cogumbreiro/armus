package pt.ul.armus.edgebuffer;

import java.util.concurrent.atomic.AtomicReference;

import pt.ul.armus.deadlockresolver.DeadlockFoundException;

/**
 * This is the base task handle. The implementation uses an atomic reference
 * to hold the blocked {@link EdgeSet}.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
class BaseTaskHandle implements TaskHandle {
	/**
	 * The blocked edges.
	 */
	private AtomicReference<EdgeSet> blocked = new AtomicReference<>();
	private boolean isBlocked = false;

	@Override
	public void setBlocked(EdgeSet newEdges) throws DeadlockFoundException {
		if (blocked.get() != null) {
			throw new IllegalStateException("Call clearBlocked() first");
		}
		blocked.set(newEdges);
		isBlocked = true;
	}

	@Override
	public void clearBlocked() {
		if (blocked.get() == null) {
			throw new IllegalStateException("Call setBlocked() first");
		}
		blocked.set(null);
		isBlocked = false;
	}

	@Override
	public boolean isBlocked() {
		return isBlocked;
	}

	@Override
	public EdgeSet getBlocked() {
		return blocked.get();
	}

	@Override
	public String toString() {
		return String.format("TaskHandle[hash=0x%x, blocked=%s]", hashCode(), blocked.get());
	}
}
