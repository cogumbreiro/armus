package pt.ul.armus.edgebuffer;

/**
 * Creates task handles.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public interface TaskHandleFactory {
	TaskHandle createTaskHandle();
}
