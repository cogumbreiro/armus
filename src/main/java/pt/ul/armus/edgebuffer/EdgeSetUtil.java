package pt.ul.armus.edgebuffer;

import java.util.Collection;
import java.util.HashSet;

import pt.ul.armus.Resource;

/**
 * Utility methods.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class EdgeSetUtil {
	/**
	 * Returns true if any resources in a collection are in the edge set. 
	 * @param edgeSet
	 * @param resources
	 * @return
	 */
	public static boolean intersectsDeadlock(EdgeSet edgeSet, Collection<Resource> resources) {
		HashSet<Resource> edgeResources = new HashSet<>();
		edgeResources.addAll(edgeSet.getRequested());
		edgeResources.retainAll(resources);
		return edgeResources.size() > 0;
	}
}
