package pt.ul.armus.edgebuffer;

import java.util.Collection;

import pt.ul.armus.Resource;

/**
 * Contains an optimized implementation of groups of precedes.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class EdgeSetFactory {
	
	private static Resource[] toArray(Resource r) {
		return new Resource[]{r};
	}
	
	private static Resource[] toArray(Collection<Resource> r) {
		return r.toArray(new Resource[r.size()]);
	}
	// ------------------------------------------------
	
	/**
	 * Creates a restriction where in the right-hand side is a collection.
	 * 
	 * @param activity
	 * @param request
	 * @param allocate
	 * @return
	 */
	public static EdgeSet requestOneAllocateMany(Resource request,
			Collection<Resource> allocate) {
		return new ManyToManyGroup(toArray(request), toArray(allocate));
	}

	/**
	 * Creates a restriction where in the left-hand side is a collection.
	 * 
	 * @param activity
	 * @param left
	 * @param rightCollection
	 * @return
	 */
	public static EdgeSet requestManyAllocateOne(
			Collection<Resource> request, Resource allocate) {
		return new ManyToManyGroup(toArray(request), toArray(allocate));
	}

	/**
	 * Creates a restriction with two variables.
	 * 
	 * @param activity
	 * @param request
	 * @param allocate
	 * @return
	 */
	public static EdgeSet requestOneAllocateOne(Resource request, Resource allocate) {
		return new ManyToManyGroup(toArray(request),
				toArray(allocate));
	}
	
	/**
	 * When multiple resources succeed another collection of resources.
	 * @param requested
	 * @param allocated
	 * @return
	 */
	public static EdgeSet requestManyAllocateMany(Collection<Resource> requested, Collection<Resource> allocated) {
		return new ManyToManyGroup(toArray(requested), toArray(allocated));
	}

	/**
	 * Many to many relation.
	 * @param request
	 * @param allocate
	 * @return
	 */
	public static EdgeSet requestManyAllocateManyArray(Resource[] request, Resource[] allocate) {
		return new ManyToManyGroup(request, allocate);
	}
}
