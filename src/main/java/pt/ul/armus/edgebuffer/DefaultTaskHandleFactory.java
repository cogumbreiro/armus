package pt.ul.armus.edgebuffer;
/**
 * The default task handle factory uses a simple synchronized data structure.
 * @see BaseTaskHandle
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class DefaultTaskHandleFactory implements TaskHandleFactory {
	/**
	 * Default singleton.
	 */
	public static final TaskHandleFactory DEFAULT = new DefaultTaskHandleFactory();

	@Override
	public TaskHandle createTaskHandle() {
		return new BaseTaskHandle();
	}
}
