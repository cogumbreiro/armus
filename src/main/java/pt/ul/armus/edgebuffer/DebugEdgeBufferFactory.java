package pt.ul.armus.edgebuffer;

import java.util.logging.Logger;

import pt.ul.armus.deadlockresolver.DeadlockFoundException;

/**
 * For debugging purposes only.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class DebugEdgeBufferFactory implements EdgeBufferFactory {
	private static final Logger LOG = Logger.getLogger(DebugEdgeBufferFactory.class.getSimpleName());
	
	@Override
	public EdgeBuffer createEdgeBuffer() {
		return new QueueEdgeBuffer(new TaskHandleFactory() {
			@Override
			public TaskHandle createTaskHandle() {
				return new BaseTaskHandle() {
					@Override
					public void setBlocked(EdgeSet group) throws DeadlockFoundException {
						LOG.warning(String.format("%s#setBlocked: %s", this, group));
						super.setBlocked(group);
					}
				};
			}
		});
	}
}
