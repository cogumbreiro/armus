package pt.ul.armus.cycledetector.jacop;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import pt.ul.armus.Edge;
import pt.ul.armus.cycledetector.CycleDetector;
import JaCoP.constraints.XltY;
import JaCoP.core.IntVar;
import JaCoP.core.Store;
import JaCoP.search.DepthFirstSearch;
import JaCoP.search.IndomainMin;
import JaCoP.search.InputOrderSelect;
import JaCoP.search.Search;
import JaCoP.search.SelectChoicePoint;

public class JaCoPStore implements CycleDetector {

	@Override
	public <T> Collection<T> detect(Collection<Edge<T>> edges) {
		Store store = new Store();
		Set<T> knownVertices = new HashSet<>();
		for (Edge<T> prec : edges) {
			knownVertices.add(prec.getHead());
			knownVertices.add(prec.getTail());
		}
		final int size = knownVertices.size();
		Map<T, IntVar> barrToVar = new HashMap<>();
		Map<IntVar, T> varToBarr = new HashMap<>();
		int i = 0;
		IntVar[] vars = new IntVar[size];
		for (T vertex : knownVertices) {
			IntVar var = new IntVar(store, "b" + (i + 1), 1, size);
			barrToVar.put(vertex, var);
			varToBarr.put(var, vertex);
			vars[i] = var;
			i++;
		}
		for (Edge<T> prec : edges) {
			IntVar first = barrToVar.get(prec.getHead());
			IntVar second = barrToVar.get(prec.getTail());
			store.impose(new XltY(first, second));
		}
        Search<IntVar> search = new DepthFirstSearch<IntVar>(); 
        SelectChoicePoint<IntVar> select = 
            new InputOrderSelect<IntVar>(store, vars, 
                                         new IndomainMin<IntVar>()); 
        if (!search.labeling(store, select)) {
        	return knownVertices;
        }
        return null;
	}
}
