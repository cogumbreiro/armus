package pt.ul.armus.cycledetector.jgraph;

import java.util.Collection;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import pt.ul.armus.Edge;
import pt.ul.armus.cycledetector.CycleDetector;


/**
 * Uses JGraphT for cycle detection.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 * @param <R>
 */
public class JGraphTSolver implements CycleDetector {

	@Override
	public <T> Collection<T> detect(Collection<Edge<T>> edges) {
		assert edges != null;
		DefaultDirectedGraph<T, DefaultEdge> digraph = new DefaultDirectedGraph<>(
				DefaultEdge.class);
		for (Edge<T> edge : edges) {
			final T first = edge.getHead();
			final T second = edge.getTail();
			digraph.addVertex(first);
			digraph.addVertex(second);
			digraph.addEdge(first, second);
		}
		org.jgrapht.alg.CycleDetector<T, DefaultEdge> detector = new org.jgrapht.alg.CycleDetector<>(
				digraph);
		if (detector.detectCycles()) {
        	return detector.findCycles();
		}
		return null;
	}

}
