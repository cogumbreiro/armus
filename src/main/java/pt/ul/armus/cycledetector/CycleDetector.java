package pt.ul.armus.cycledetector;

import java.util.Collection;

import pt.ul.armus.Edge;

/**
 * Finds a cycle in a graph.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@di.fc.ul.pt)
 * 
 * @param <R>
 *            The type of the resource being used.
 */
public interface CycleDetector {
	/**
	 * Returns a collection of resources that constitute a cycle.
	 * 
	 * @param edges
	 * @return null if there is no cycle.
	 */
	<T> Collection<T> detect(Collection<Edge<T>> edges);
}
