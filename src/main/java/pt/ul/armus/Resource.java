package pt.ul.armus;

/**
 * A variable, to be used by the {@link EdgeStore}.
 * 
 * The code must implement {@link Object#equals(Object)} and
 * {@link Object#hashCode()}.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@di.fc.ul.pt)
 * 
 */
public interface Resource {
	/**
	 * Gets the synchronization object.
	 * @return
	 */
	Object getSynch();
	/**
	 * It is crucial to implement the equals method correctly.
	 * @param obj
	 * @return
	 */
	@Override
	public boolean equals(Object obj);
	
	/**
	 * It is crucial to implement the hashCode method correctly.
	 * @return
	 */
	@Override
	public int hashCode();
}
