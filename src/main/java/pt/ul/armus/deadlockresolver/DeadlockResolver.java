package pt.ul.armus.deadlockresolver;

import pt.ul.armus.DeadlockInfo;

/**
 * This object will handle when a deadlock is found.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 * @param <V>
 */
public interface DeadlockResolver {
	/**
	 * Handle when a deadlock is found.
	 * @param deadlock The variables involved in the deadlock. 
	 */
	void onDeadlockDetected(DeadlockInfo deadlock);
}
