package pt.ul.armus.deadlockresolver;

import pt.ul.armus.DeadlockInfo;

/**
 * Thrown when a deadlock is found.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class DeadlockFoundException extends Exception {

	private static final long serialVersionUID = -388068807640492791L;

	private DeadlockInfo deadlock;
	
	public DeadlockFoundException(DeadlockInfo deadlock) {
		this.deadlock = deadlock;
	}

	public DeadlockFoundException(DeadlockInfo deadlock, String message) {
		super(message);
		this.deadlock = deadlock;
	}

	public DeadlockFoundException(DeadlockInfo deadlock, Throwable cause) {
		super(cause);
		this.deadlock = deadlock;
	}

	public DeadlockInfo getDeadlock() {
		return deadlock;
	}
}
