package pt.ul.armus.deadlockresolver;

import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ul.armus.DeadlockInfo;

/**
 * A deadlock resolver that logs the deadlock under at {@link Level#SEVERE}
 * and then terminates the virtual machine with error code 255.
 */
public class FatalDeadlockResolver implements DeadlockResolver {
	private final static Logger LOGGER = Logger.getLogger(DeadlockResolver.class.getName());
	@Override
	public void onDeadlockDetected(DeadlockInfo deadlocked) {
		LOGGER.log(Level.SEVERE, "Deadlock detected: " + deadlocked);
		System.exit(255);
	}
}
