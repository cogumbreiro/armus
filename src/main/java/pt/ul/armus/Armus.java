package pt.ul.armus;

import pt.ul.armus.edgebuffer.EdgeBuffer;


/**
 * Controls the internals of the strategy.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public interface Armus {
	/**
	 * Start the algorithm.
	 */
	void start();

	/**
	 * Obtains the edge buffer.
	 * @return
	 */
	EdgeBuffer getBuffer();
	
	/**
	 * Checks for the existence of a deadlock.
	 * 
	 * @return null if there is no deadlock.
	 */
	DeadlockInfo checkDeadlock();
	
	/**
	 * Stop the algorithm.
	 */
	void stop();
}
