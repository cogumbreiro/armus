package pt.ul.armus;

import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ul.armus.edgebuffer.EdgeBuffer;

/**
 * A deadlock detection algorithm.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
class ClearBuffer implements Component {
	private static Logger LOG = Logger.getLogger(ClearBuffer.class.getName());

	private EdgeBuffer buffer;

	@Override
	public void init(StrategyController ctx) {
		buffer = ctx.getEdgeBuffer();
	}

	@Override
	public void onStart() {
		LOG.log(Level.CONFIG, "verification strategy started");
	}

	@Override
	public void onStop() {
		buffer.clear();
	}
}
