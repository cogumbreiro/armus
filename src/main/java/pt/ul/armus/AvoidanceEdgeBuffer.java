package pt.ul.armus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import pt.ul.armus.edgebuffer.EdgeBuffer;
import pt.ul.armus.edgebuffer.EdgeSet;
import pt.ul.armus.edgebuffer.TaskHandle;

public class AvoidanceEdgeBuffer implements EdgeBuffer {
	private final EdgeBuffer delegate;
	private StrategyController controller;
	
	public AvoidanceEdgeBuffer(EdgeBuffer delegate) {
		this.delegate = delegate;
	}
	
	public StrategyController getController() {
		return controller;
	}

	public void setController(StrategyController controller) {
		this.controller = controller;
	}

	public TaskHandle adapt(TaskHandle handle) {
		return new AvoidanceTaskHandle(handle, controller);
	}
	
	@Override
	public TaskHandle createHandle() {
		return adapt(delegate.createHandle());
	}

	@Override
	public void clear() {
		delegate.clear();
	}

	@Override
	public void destroy(TaskHandle handle) {
		delegate.destroy(((AvoidanceTaskHandle) handle).getDelegate()) ;
	}

	@Override
	public Collection<EdgeSet> getBlockedEdges() {
		return delegate.getBlockedEdges();
	}

	@Override
	public Collection<TaskHandle> getHandles() {
		Collection<TaskHandle> result = new ArrayList<>();
		for (TaskHandle handle : delegate.getHandles()) {
			result.add(adapt(handle));
		}
		return result;
	}

	@Override
	public Map<TaskHandle, EdgeSet> getAll() {
		Map<TaskHandle, EdgeSet> snapshot = new HashMap<>();
		for (Entry<TaskHandle, EdgeSet> entry : delegate.getAll().entrySet()) {
			snapshot.put(adapt(entry.getKey()), entry.getValue());
		}
		return snapshot;
	}

}
