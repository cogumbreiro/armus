package pt.ul.armus;

import pt.ul.armus.edgebuffer.EdgeBuffer;

/**
 * Controls the internals of the strategy.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
interface StrategyController {
	/**
	 * Obtains the buffer that holds the state of the verification.
	 * 
	 * @return
	 */
	EdgeBuffer getEdgeBuffer();

	/**
	 * Checks for the existence of a deadlock.
	 * 
	 * @return null if there is no deadlock.
	 */
	DeadlockInfo checkDeadlock();
}
