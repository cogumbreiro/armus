package pt.ul.armus;

import java.util.Collection;

import pt.ul.armus.edgebuffer.EdgeSetUtil;
import pt.ul.armus.edgebuffer.TaskHandle;

public class ResourceDeadlockInfo implements DeadlockInfo {
	public final Collection<Resource> deadlock;

	/**
	 * @param deadlock
	 */
	public ResourceDeadlockInfo(Collection<Resource> deadlock) {
		this.deadlock = deadlock;
	}

	@Override
	public boolean affects(TaskHandle handle) {
		return EdgeSetUtil.intersectsDeadlock(handle.getBlocked(), deadlock);
	}
	
	@Override
	public String toString() {
		return deadlock.toString();
	}
}
