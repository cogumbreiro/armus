package pt.ul.armus.util;

import java.util.Properties;

import static pt.ul.armus.util.StringUtils.join;

/**
 * Define utility functions to manipulate {@link Properties} objects.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class PropertiesUtils {
	/**
	 * The separator used in property files.
	 */
	public static final String SEPARATOR = ".";

	/**
	 * Construct an hierarchic key.
	 * 
	 * @param parts
	 * @return
	 */
	public static String joinKeys(String... parts) {
		return join(SEPARATOR, parts);
	}

	/**
	 * Copies all properties with a given prefix, and removes that prefix for
	 * them.
	 * 
	 * @param props
	 * @param prefix
	 * @return
	 */
	public static Properties copyPrefix(Properties props, String prefix) {
		Properties target = new Properties();
		for (String key : props.stringPropertyNames()) {
			if (key.startsWith(prefix)) {
				target.setProperty(key.substring(prefix.length()),
						props.getProperty(key));
			}
		}
		return target;
	}

	/**
	 * Merges the given properties into the {@link System#getProperties()}.
	 * 
	 * @param props
	 */
	public static void copyInto(Properties source, Properties target) {
		for (String key : source.stringPropertyNames()) {
			target.setProperty(key, source.getProperty(key));
		}
	}

	/**
	 * Merges the given properties into the {@link System#getProperties()}.
	 * 
	 * @param props
	 */
	public static void mergeToSystemProperties(Properties props) {
		copyInto(props, System.getProperties());
	}

}
