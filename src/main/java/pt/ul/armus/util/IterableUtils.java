package pt.ul.armus.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class IterableUtils {
	private static class KeyVal<K,V> implements Map.Entry<K, V> {
		private final K key;
		private final V value;
		
		public KeyVal(K key, V value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public K getKey() {
			return key;
		}

		@Override
		public V getValue() {
			return value;
		}

		@Override
		public V setValue(V value) {
			throw new UnsupportedOperationException();
		}
		
	}
	
	private static class KeyValIterator<K, V> implements Iterator<Map.Entry<K, V>> {
		private final Iterator<K> keysIter;
		private final Iterator<V> valuesIter;
		
		public KeyValIterator(Iterator<K> keysIter, Iterator<V> valuesIter) {
			this.keysIter = keysIter;
			this.valuesIter = valuesIter;
		}

		@Override
		public boolean hasNext() {
			return keysIter.hasNext() && valuesIter.hasNext();
		}

		@Override
		public Entry<K, V> next() {
			return new KeyVal<>(keysIter.next(), valuesIter.next());
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}
	public static <K, V> Iterable<Map.Entry<K, V>> zip(final Iterable<K> keys, final Iterable<V> values) {
		return new Iterable<Map.Entry<K,V>>() {
			@Override
			public Iterator<Entry<K, V>> iterator() {
				return new KeyValIterator<K, V>(keys.iterator(), values.iterator());
			}
		};
	}
}
