package pt.ul.armus.util;

/**
 * Define utility functions to manipulate {@link String} objects.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class StringUtils {
	/**
	 * Join an array of strings by placing a separator in between each two
	 * elements of the array.
	 * 
	 * @param separator
	 * @param parts
	 * @return
	 */
	public static String join(String separator, String... parts) {
		int count = 0;
		StringBuilder builder = new StringBuilder();
		for (String part : parts) {
			if (count > 0) {
				builder.append(separator);
			}
			builder.append(part);
			count++;
		}
		return builder.toString();
	}
}
