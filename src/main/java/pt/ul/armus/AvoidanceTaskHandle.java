package pt.ul.armus;

import pt.ul.armus.deadlockresolver.DeadlockFoundException;
import pt.ul.armus.edgebuffer.EdgeSet;
import pt.ul.armus.edgebuffer.TaskHandle;

/**
 * Performs deadlock avoidance on a specific task handle.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
class AvoidanceTaskHandle implements TaskHandle {
	private final StrategyController controller;
	private final TaskHandle delegate;
	
	public AvoidanceTaskHandle(TaskHandle delegate, StrategyController verifier) {
		assert delegate != null;
		assert verifier != null;
		this.delegate = delegate;
		this.controller = verifier;
	}

	@Override
	public void setBlocked(EdgeSet newEdges) throws DeadlockFoundException {
		delegate.setBlocked(newEdges);
		DeadlockInfo deadlock = controller.checkDeadlock();
		if (deadlock != null && deadlock.affects(this)) {
			throw new DeadlockFoundException(deadlock);
		}
	}

	public boolean isBlocked() {
		return delegate.isBlocked();
	}

	public EdgeSet getBlocked() {
		return delegate.getBlocked();
	}

	public void clearBlocked() {
		delegate.clearBlocked();
	}
	
	/**
	 * The task handle we are avoiding deadlocks.
	 * @return
	 */
	public TaskHandle getDelegate() {
		return delegate;
	}
	
	/**
	 * The algorithm that checks for deadlocks.
	 * @return
	 */
	public StrategyController getController() {
		return controller;
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}
	
	// makes sure an avoidance task does not interfere in the equals
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		return obj.equals(delegate);
	}
	
}
