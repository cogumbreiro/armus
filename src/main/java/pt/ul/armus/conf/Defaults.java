package pt.ul.armus.conf;

import pt.ul.armus.conf.in.Parser;
import pt.ul.armus.conf.in.ParserException;
import pt.ul.armus.conf.in.PrimitiveParser;
import pt.ul.armus.conf.in.SpecializedParser;
import pt.ul.armus.conf.out.PrimitiveSerializer;
import pt.ul.armus.conf.out.PrimitiveSerializerFactory;
import pt.ul.armus.conf.out.Serializer;
import pt.ul.armus.conf.out.SpecializedSerializer;
import pt.ul.armus.cycledetector.CycleDetector;
import pt.ul.armus.deadlockresolver.DeadlockResolver;
import pt.ul.armus.edgebuffer.EdgeBufferFactory;
import static pt.ul.armus.conf.in.ParserUtil.parseClass;

class Defaults {
	public static final String PREFIX = "armus";
	public static final String BUFFER_CLEAR_AT_EXIT = "clear_at_exit";
	public static final String BUFFER_CLASS = "class";

	public static final SpecializedParser<EdgeBufferConfiguration> EDGE_BUFFER_IN = new SpecializedParser<EdgeBufferConfiguration>() {
		@Override
		public EdgeBufferConfiguration parse(Parser objParser,
				PrimitiveParser parser) throws ParserException {
			boolean clearBufferAtTheEnd = parser
					.parseBoolean(BUFFER_CLEAR_AT_EXIT);
			EdgeBufferFactory factory;
			try {
				factory = parseClass(parser.parseString(BUFFER_CLASS),
						EdgeBufferFactory.class).newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new ParserException(e);
			}
			return new EdgeBufferConfiguration(clearBufferAtTheEnd, factory);
		}
	};

	public static final SpecializedSerializer<EdgeBufferConfiguration> EDGE_BUFFER_OUT = new SpecializedSerializer<EdgeBufferConfiguration>() {
		@Override
		public <T> T serialize(EdgeBufferConfiguration source,
				Serializer complexOut, PrimitiveSerializerFactory<T> factory) {
			PrimitiveSerializer<T> out = factory.create();
			out.putBoolean(BUFFER_CLEAR_AT_EXIT, source.clearBufferAtTheEnd);
			out.putString(BUFFER_CLASS, source.factory.getClass().getName());
			return out.serialize();
		}
	};

	public static final String DETECTION_ENABLED = "enabled";
	public static final String DETECTION_DELAY = "delay";
	public static final String DETECTION_PERIOD = "period";
	public static final SpecializedParser<DetectionConfiguration> DETECTION_IN = new SpecializedParser<DetectionConfiguration>() {
		@Override
		public DetectionConfiguration parse(Parser objParser,
				PrimitiveParser conf) throws ParserException {
			boolean isEnabled = conf.parseBoolean(DETECTION_ENABLED);
			long delay = conf.parseLong(DETECTION_DELAY);
			long period = conf.parseLong(DETECTION_PERIOD);
			return new DetectionConfiguration(isEnabled, delay, period);
		}
	};
	public static final SpecializedSerializer<DetectionConfiguration> DETECTION_OUT = new SpecializedSerializer<DetectionConfiguration>() {
		@Override
		public <T> T serialize(DetectionConfiguration source,
				Serializer complexOut, PrimitiveSerializerFactory<T> factory) {
			PrimitiveSerializer<T> out = factory.create();
			out.putBoolean(DETECTION_ENABLED, source.isEnabled);
			out.putLong(DETECTION_DELAY, source.delay);
			out.putLong(DETECTION_PERIOD, source.period);
			return out.serialize();
		}
	};

	public static final String BUFFER_PREFIX = "buffer";
	public static final String DETECTION_PREFIX = "detection";
	public static final String AVOIDANCE_ENABLED = "avoidance.enabled";
	public static final String GRAPH = "graph";
	public static final String CYCLE_DETECTOR = "cycle_detector.class";
	public static final String DEADLOCK_RESOLVER = "deadlock_resolver.class";
	public static final String PRINT_CONFIG = "print_config";
	public static final SpecializedParser<MainConfiguration> MAIN_IN = new SpecializedParser<MainConfiguration>() {
		@Override
		public MainConfiguration parse(Parser objParser, PrimitiveParser conf)
				throws ParserException {
			DetectionConfiguration dConf = objParser.parse(
					conf.parseConfiguration(DETECTION_PREFIX),
					DetectionConfiguration.class);
			EdgeBufferConfiguration eConf = objParser.parse(
					conf.parseConfiguration(BUFFER_PREFIX),
					EdgeBufferConfiguration.class);
			boolean avoidEnable = conf.parseBoolean(AVOIDANCE_ENABLED);
			GraphStrategy graph = GraphStrategy.valueOf(conf.parseString(GRAPH).trim().toUpperCase());
			boolean printConfig = conf.parseBoolean(PRINT_CONFIG);
			CycleDetector detector;
			DeadlockResolver resolver;
			try {
				detector = parseClass(conf.parseString(CYCLE_DETECTOR),
						CycleDetector.class).newInstance();
				resolver = parseClass(conf.parseString(DEADLOCK_RESOLVER),
						DeadlockResolver.class).newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new ParserException(e);
			}
			return new MainConfiguration(dConf, eConf, avoidEnable, detector,
					resolver, graph, printConfig);
		}
	};
	public static final SpecializedSerializer<MainConfiguration> MAIN_OUT = new SpecializedSerializer<MainConfiguration>() {
		@Override
		public <T> T serialize(MainConfiguration source, Serializer complexOut,
				PrimitiveSerializerFactory<T> factory) {
			PrimitiveSerializer<T> out = factory.create();
			T detectionOut = complexOut.serialize(DetectionConfiguration.class, source.detection, factory);
			out.putStructured(DETECTION_PREFIX, detectionOut);
			T bufferOut = complexOut.serialize(EdgeBufferConfiguration.class, source.edgeBuffer, factory);
			out.putStructured(BUFFER_PREFIX, bufferOut);
			out.putBoolean(AVOIDANCE_ENABLED, source.avoidanceEnabled);
			out.putString(CYCLE_DETECTOR, source.cycleDetector.getClass()
					.getName());
			out.putString(DEADLOCK_RESOLVER, source.deadlockResolver.getClass()
					.getName());
			out.putString(GRAPH, source.graphStrategy.name().toLowerCase());
			out.putBoolean(PRINT_CONFIG, source.printConfig);
			return out.serialize();
		}
	};
}
