package pt.ul.armus.conf.in;

import java.util.HashMap;
import java.util.Map;

public class ParserImpl implements Parser {
	private Map<Class<?>, SpecializedParser<?>> parsers = new HashMap<>();
	
	@Override
	public <T> T parse(PrimitiveParser conf, Class<T> cls)
			throws ParserException {
		SpecializedParser<T> parser = get(cls);
		if (parser == null) {
			throw new ParserException("Unsupported class: " + cls.getName());
		}
		return parser.parse(this, conf);
	}
	
	@SuppressWarnings("unchecked")
	private <T> SpecializedParser<T> get(Class<T> cls) {
		return (SpecializedParser<T>) parsers.get(cls);
	}

	/**
	 * Registers a specific parser.
	 * 
	 * @param cls
	 * @param p
	 */
	public <T> void register(Class<T> cls, SpecializedParser<T> p) {
		parsers.put(cls, p);
	}

	/**
	 * Deregisters a specific parser.
	 * 
	 * @param cls
	 * @return The existing parser.
	 */
	@SuppressWarnings("unchecked")
	public <T> SpecializedParser<T> deregister(Class<T> cls) {
		return (SpecializedParser<T>) parsers.remove(cls);
	}

}
