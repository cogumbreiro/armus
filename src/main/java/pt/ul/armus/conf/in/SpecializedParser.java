package pt.ul.armus.conf.in;

/**
 * Parses only objects of a specific type.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 * @param <T>
 *            The type of the parsed objects.
 */
public interface SpecializedParser<T> {
	/**
	 * Parse an instance that this parser supports.
	 * 
	 * @param parser
	 *            Used to construct more complex objects.
	 * @param basicParser
	 *            Primitive values should be looked up here.
	 * @return
	 * @throws ParserException
	 */
	public T parse(Parser parser, PrimitiveParser basicParser)
			throws ParserException;
}
