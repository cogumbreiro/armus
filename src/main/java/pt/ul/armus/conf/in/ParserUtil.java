package pt.ul.armus.conf.in;

public class ParserUtil {
	public static <T> Class<? extends T> parseClass(final String value,
			Class<T> baseClass) throws ParserException {
		try {
			return ClassLoader.getSystemClassLoader().loadClass(value)
					.asSubclass(baseClass);
		} catch (ClassNotFoundException e) {
			throw new ParserException(e);
		}
	}
}
