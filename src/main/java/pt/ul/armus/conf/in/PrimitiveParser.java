package pt.ul.armus.conf.in;

/**
 * Parses the primitive values from a configuration. It is also possible to
 * obtain a sub-configuration at a specific key.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public interface PrimitiveParser {
	/**
	 * Parses a boolean value.
	 * 
	 * @param key
	 * @return
	 * @throws ParserException
	 */
	boolean parseBoolean(String key) throws ParserException;

	/**
	 * Parses a string value.
	 * 
	 * @param key
	 * @return
	 * @throws ParserException
	 */
	String parseString(String key) throws ParserException;

	/**
	 * Parses an integer value.
	 * 
	 * @param key
	 * @return
	 * @throws ParserException
	 */
	int parseInt(String key) throws ParserException;

	/**
	 * Parses a long value.
	 * 
	 * @param key
	 * @return
	 * @throws ParserException
	 */
	long parseLong(String key) throws ParserException;

	/**
	 * Parses a configuration. Can also be seen as a way to obtain a child of
	 * the configuration tree.
	 * 
	 * @param key
	 * @return
	 * @throws ParserException
	 */
	PrimitiveParser parseConfiguration(String key) throws ParserException;
}
