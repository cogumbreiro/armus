package pt.ul.armus.conf.in;

/**
 * Uses a configuration to construct objects.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public interface Parser {
	/**
	 * Parses a given configuration and parses it to obtain an instance of the
	 * given class.
	 * @param conf
	 * @param cls
	 * @return
	 * @throws ParserException
	 */
	<T> T parse(PrimitiveParser conf, Class<T> cls)
			throws ParserException;
}
