package pt.ul.armus.conf.in;

import java.util.Properties;

import pt.ul.armus.util.PropertiesUtils;

public class PropertiesConfigurationParser implements PrimitiveParser {
	private Properties conf;
	
	public PropertiesConfigurationParser(Properties conf) {
		this.conf = conf;
	}

	private String get(String key) throws ParserException {
		String result = conf.getProperty(key);
		if (result == null) {
			throw new ParserException("Key not found: " + key);
		}
		return result;
	}
	
	@Override
	public boolean parseBoolean(String key) throws ParserException {
		return Boolean.parseBoolean(get(key));
	}

	@Override
	public String parseString(String key) throws ParserException {
		return get(key);
	}

	@Override
	public int parseInt(String key) throws ParserException {
		try {
			return Integer.valueOf(get(key));
		} catch (NumberFormatException e) {
			throw new ParserException(e);
		}
	}

	@Override
	public long parseLong(String key) throws ParserException {
		try {
			return Long.valueOf(get(key));
		} catch (NumberFormatException e) {
			throw new ParserException(e);
		}
	}

	@Override
	public PrimitiveParser parseConfiguration(String key)
			throws ParserException {
		String prefix = key + PropertiesUtils.SEPARATOR;
		Properties newMap = PropertiesUtils.copyPrefix(conf, prefix);
		return new PropertiesConfigurationParser(newMap);
	}

}
