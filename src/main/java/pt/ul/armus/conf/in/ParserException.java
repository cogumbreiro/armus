package pt.ul.armus.conf.in;

public class ParserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8304502462260520162L;

	/**
	 * 
	 */
	public ParserException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ParserException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ParserException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ParserException(Throwable cause) {
		super(cause);
	}
}
