package pt.ul.armus.conf;

import pt.ul.armus.edgebuffer.EdgeBufferFactory;

/**
 * Configuration that relates to the edge buffer component.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class EdgeBufferConfiguration implements Cloneable {
	/**
	 * Remove all edges in the edge buffer when we exit the program?
	 */
	public boolean clearBufferAtTheEnd;
	/**
	 * The factory of the edge buffer.
	 */
	public EdgeBufferFactory factory;

	/**
	 * @param clearBufferAtTheEnd
	 * @param edgeBufferFactory
	 */
	public EdgeBufferConfiguration(boolean clearBufferAtTheEnd,
			EdgeBufferFactory edgeBufferFactory) {
		this.clearBufferAtTheEnd = clearBufferAtTheEnd;
		this.factory = edgeBufferFactory;
	}

	@Override
	public String toString() {
		return "EdgeBufferConfiguration [clearBufferAtTheEnd="
				+ clearBufferAtTheEnd + ", factory=" + factory + "]";
	}
	
	@Override
	public EdgeBufferConfiguration clone() {
		return new EdgeBufferConfiguration(clearBufferAtTheEnd, factory);
	}
}
