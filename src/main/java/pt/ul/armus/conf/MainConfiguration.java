package pt.ul.armus.conf;

import pt.ul.armus.cycledetector.CycleDetector;
import pt.ul.armus.deadlockresolver.DeadlockResolver;

/**
 * The main configuration of armus.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class MainConfiguration implements Cloneable {
	/**
	 * The configuration of the deadlock detection algorithm.
	 */
	public DetectionConfiguration detection;
	/**
	 * The configuration of the edge buffer component.
	 */
	public EdgeBufferConfiguration edgeBuffer;
	/**
	 * Runs the deadlock avoidance algorithm?
	 */
	public boolean avoidanceEnabled;
	/**
	 * The factory for the cycle detector.
	 */
	public CycleDetector cycleDetector;
	/**
	 * What do we do when we find a deadlock.
	 */
	public DeadlockResolver deadlockResolver;
	/**
	 * Checks if we should use the WFG for cycle detection, instead of the SG.
	 */
	public GraphStrategy graphStrategy;
	/**
	 * Prints the configuration
	 */
	public boolean printConfig;

	/**
	 * The default constructor.
	 * 
	 * @param detection
	 * @param edgeBuffer
	 * @param avoidanceEnabled
	 * @param clearBufferAtTheEnd
	 * @param cycleDetector
	 * @param edgeBufferFactory
	 * @param deadlockResolver
	 */
	public MainConfiguration(DetectionConfiguration detection,
			EdgeBufferConfiguration edgeBuffer, boolean avoidanceEnabled,
			CycleDetector cycleDetector, DeadlockResolver deadlockResolver,
			GraphStrategy graphStrategy, boolean printConfig) {
		this.detection = detection;
		this.edgeBuffer = edgeBuffer;
		this.avoidanceEnabled = avoidanceEnabled;
		this.cycleDetector = cycleDetector;
		this.deadlockResolver = deadlockResolver;
		this.graphStrategy = graphStrategy;
		this.printConfig = printConfig;
	}

	@Override
	public String toString() {
		return "MainConfiguration [detection=" + detection + ", edgeBuffer="
				+ edgeBuffer + ", avoidanceEnabled=" + avoidanceEnabled
				+ ", cycleDetector=" + cycleDetector + ", deadlockResolver="
				+ deadlockResolver + ", graph=" + graphStrategy + ", printConfig=" + printConfig + "]";
	}

	@Override
	public MainConfiguration clone() {
		return new MainConfiguration(detection.clone(), edgeBuffer.clone(),
				avoidanceEnabled, cycleDetector, deadlockResolver, graphStrategy, printConfig);
	}
}