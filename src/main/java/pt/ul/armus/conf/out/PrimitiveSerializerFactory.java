package pt.ul.armus.conf.out;

public interface PrimitiveSerializerFactory<T> {
	PrimitiveSerializer<T> create();
}
