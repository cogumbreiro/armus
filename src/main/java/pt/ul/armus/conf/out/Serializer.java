package pt.ul.armus.conf.out;

public interface Serializer {
	<S,T> T serialize(Class<S> cls, S source, PrimitiveSerializerFactory<T> conf);
}
