package pt.ul.armus.conf.out;

public interface SpecializedSerializer<S> {
	<T> T serialize(S source, Serializer serializer, PrimitiveSerializerFactory<T> primitiveSerializer);
}
