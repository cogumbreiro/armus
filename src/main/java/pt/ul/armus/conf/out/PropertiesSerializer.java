package pt.ul.armus.conf.out;

import java.util.Properties;

public class PropertiesSerializer implements PrimitiveSerializer<Properties> {
	private final String SEP = ".";
	private final Properties props = new Properties();

	@Override
	public void putBoolean(String key, boolean value) {
		props.put(key, Boolean.toString(value));
	}

	@Override
	public void putString(String key, String value) {
		props.put(key, value);
	}

	@Override
	public void putInt(String key, int value) {
		props.put(key, Integer.toString(value));
	}

	@Override
	public void putLong(String key, long value) {
		props.put(key, Long.toString(value));
	}

	@Override
	public void putStructured(String key, Properties newProps) {
		for (String subKey : newProps.stringPropertyNames()) {
			props.put(key + SEP + subKey, newProps.getProperty(subKey));
		}
	}

	@Override
	public Properties serialize() {
		// send a copy
		Properties result = new Properties();
		for (String key : props.stringPropertyNames()) {
			result.put(key, props.getProperty(key));
		}
		return result;
	}
}
