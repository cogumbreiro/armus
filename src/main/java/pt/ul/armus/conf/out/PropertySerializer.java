package pt.ul.armus.conf.out;

import java.util.Properties;

public class PropertySerializer implements PrimitiveSerializerFactory<Properties> {
	@Override
	public PrimitiveSerializer<Properties> create() {
		return new PropertiesSerializer();
	}
}
