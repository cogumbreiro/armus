package pt.ul.armus.conf.out;

import java.util.HashMap;
import java.util.Map;

public class SerializerImpl implements Serializer {
	private Map<Class<?>, SpecializedSerializer<?>> registry = new HashMap<>();
	
	@SuppressWarnings("unchecked")
	private <T> SpecializedSerializer<T> get(Class<T> cls) {
		return (SpecializedSerializer<T>) registry.get(cls);
	}

	/**
	 * Registers a specific parser.
	 * 
	 * @param cls
	 * @param p
	 */
	public <T> void register(Class<T> cls, SpecializedSerializer<T> p) {
		registry.put(cls, p);
	}

	/**
	 * Deregisters a specific parser.
	 * 
	 * @param cls
	 * @return The existing parser.
	 */
	@SuppressWarnings("unchecked")
	public <T> SpecializedSerializer<T> deregister(Class<T> cls) {
		return (SpecializedSerializer<T>) registry.remove(cls);
	}


	@Override
	public <S,T> T serialize(Class<S> cls, S source, PrimitiveSerializerFactory<T> conf) {
		SpecializedSerializer<S> ser = get(cls);
		return ser.serialize(source, this, conf);
	}

}
