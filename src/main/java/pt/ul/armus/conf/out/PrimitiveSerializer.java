package pt.ul.armus.conf.out;

public interface PrimitiveSerializer<T> {
	void putBoolean(String key, boolean value);

	void putString(String key, String value);

	void putInt(String key, int value);

	void putLong(String key, long value);

	void putStructured(String key, T section);

	T serialize(); 
}
