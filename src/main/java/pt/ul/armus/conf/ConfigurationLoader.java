package pt.ul.armus.conf;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import pt.ul.armus.conf.in.ParserException;
import pt.ul.armus.conf.in.ParserImpl;
import pt.ul.armus.conf.in.PropertiesConfigurationParser;
import pt.ul.armus.conf.out.PrimitiveSerializer;
import pt.ul.armus.conf.out.PropertySerializer;
import pt.ul.armus.conf.out.SerializerImpl;
import pt.ul.armus.cycledetector.jgraph.JGraphTSolver;
import pt.ul.armus.deadlockresolver.FatalDeadlockResolver;
import pt.ul.armus.edgebuffer.HashEdgeBufferFactory;
import pt.ul.armus.util.PropertiesUtils;

/**
 * Sets up the {@link MainConfiguration} object from the system's options.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class ConfigurationLoader {
	private static final PropertySerializer TO_PROPERTIES = new PropertySerializer();
	private static final ParserImpl in = new ParserImpl();
	private static final SerializerImpl out = new SerializerImpl();
	/**
	 * The default detection delay in miliseconds before the first run is 200
	 * miliseconds.
	 */
	private static final long DETECTION_DELAY = 1 * 200;
	/**
	 * The default detection period in miliseconds is 100 miliseconds.
	 */
	private static final long DETECTION_PERIOD = 100;
	private static final DetectionConfiguration DETECTION_DEFAULT = new DetectionConfiguration(
			false, DETECTION_DELAY, DETECTION_PERIOD);
	private static final EdgeBufferConfiguration BUFFER_DEFAULT = new EdgeBufferConfiguration(
			true, new HashEdgeBufferFactory());
	protected static final MainConfiguration DEFAULT = new MainConfiguration(
			DETECTION_DEFAULT, BUFFER_DEFAULT, true, new JGraphTSolver(),
			new FatalDeadlockResolver(), GraphStrategy.AUTO, false);
	static {
		in.register(MainConfiguration.class, Defaults.MAIN_IN);
		out.register(MainConfiguration.class, Defaults.MAIN_OUT);
		in.register(EdgeBufferConfiguration.class, Defaults.EDGE_BUFFER_IN);
		out.register(EdgeBufferConfiguration.class, Defaults.EDGE_BUFFER_OUT);
		in.register(DetectionConfiguration.class, Defaults.DETECTION_IN);
		out.register(DetectionConfiguration.class, Defaults.DETECTION_OUT);
	}

	/**
	 * The configuration is red according to the following order:
	 * <ol>
	 * <li>properties from {@link System#getProperties()}, where keys are
	 * prefixed with <code>prefix</code> and a period (.).</li>
	 * <li>properties from file "<code>prefix</code>.properties" in the current
	 * working directory</li>
	 * <li>properties rendered by {@link ConfigurationLoader#getDefault()}</li>
	 * </ol>
	 * 
	 * @return
	 * @throws ParserException
	 */
	public static MainConfiguration parseFromSystem() throws ParserException {
		return parseFromSystem(Defaults.PREFIX);
	}
	
	/**
	 * The configuration is red according to the following order:
	 * <ol>
	 * <li>properties from {@link System#getProperties()}, where keys are
	 * prefixed with <code>prefix</code> and a period (.).</li>
	 * <li>properties from file "<code>prefix</code>.properties" in the current
	 * working directory</li>
	 * <li>properties rendered by {@link ConfigurationLoader#getDefault()}</li>
	 * </ol>
	 * 
	 * @param prefix
	 *            The prefix of the keys in {@link System#getProperties()} and
	 *            the prefix of the {@link Properties} filename.
	 * @return
	 * @throws ParserException
	 */
	public static MainConfiguration parseFromSystem(String prefix) throws ParserException {
		return parseFromSystemWithDefaults(prefix, getDefault());
	}

	/**
	 * The configuration is red according to the following order:
	 * <ol>
	 * <li>properties from {@link System#getProperties()}, where keys are
	 * prefixed with <code>prefix</code> and a period (.).</li>
	 * <li>properties from file "<code>prefix</code>.properties" in the current
	 * working directory</li>
	 * <li>properties in the rendering of <code>defaultConf</code></li>
	 * </ol>
	 * 
	 * @param prefix
	 *            The prefix of the keys in {@link System#getProperties()} and
	 *            the prefix of the {@link Properties} filename.
	 * @return
	 * @throws ParserException
	 */
	public static MainConfiguration parseFromSystemWithDefaults(String prefix,
			MainConfiguration defaults) throws ParserException {
		return parseProperties(getSystemProperties(prefix, defaults));
	}

	/**
	 * Returns a properties object use to parse the {@link MainConfiguration}
	 * object.
	 * 
	 * @param prefix
	 * @return The properties without the prefix.
	 */
	public static Properties getSystemProperties(String prefix,
			MainConfiguration defaultConf) {
		Properties outcome = renderNoPrefix(defaultConf);
		try {
			Properties props = new Properties();
			props.load(new FileInputStream(prefix + ".properties"));
			PropertiesUtils.copyInto(props, outcome);
		} catch (IOException e) {
			// ok
		}
		Properties sysProps = PropertiesUtils.copyPrefix(
				System.getProperties(), prefix + PropertiesUtils.SEPARATOR);
		PropertiesUtils.copyInto(sysProps, outcome);
		return outcome;
	}
	
	/**
	 * Returns a properties object use to parse the {@link MainConfiguration}
	 * object.
	 * 
	 * @param prefix
	 * @return
	 */
	public static Properties getSystemProperties(String prefix) {
		return getSystemProperties(prefix, getDefault());
	}
	
	/**
	 * Returns a properties object use to parse the {@link MainConfiguration}
	 * object.
	 * 
	 * @return
	 */
	public static Properties getSystemProperties() {
		return getSystemProperties(Defaults.PREFIX);
	}

	/**
	 * Returns a copy of the main configuration file.
	 * 
	 * @return
	 */
	public static MainConfiguration getDefault() {
		return DEFAULT.clone();
	}

	/**
	 * Converts the configuration object to a properties file.
	 * 
	 * @param conf
	 * @return
	 */
	public static Properties renderNoPrefix(MainConfiguration conf) {
		return out.serialize(MainConfiguration.class, conf, TO_PROPERTIES);
	}

	/**
	 * Converts the configuration object to a properties file.
	 * 
	 * @param conf
	 * @return
	 */
	public static Properties render(MainConfiguration conf) {
		return render(Defaults.PREFIX, conf);
	}
	
	/**
	 * Covert a {@link MainConfiguration} to a properties object. Prefixes all
	 * keys.
	 * 
	 * @param prefix
	 * @param conf
	 * @return
	 */
	public static Properties render(String prefix, MainConfiguration conf) {
		if (prefix == null) {
			throw new IllegalArgumentException("prefix cannot be null");
		}
		Properties props = renderNoPrefix(conf);
		PrimitiveSerializer<Properties> ser = TO_PROPERTIES.create();
		ser.putStructured(prefix, props);
		return ser.serialize();
	}

	/**
	 * Creare a {@link MainConfiguration} from the given properties.
	 * 
	 * Not that the configuration parser expects <b>no prefixes</b>.
	 * 
	 * @param props
	 * @return
	 * @throws ParserException
	 */
	public static MainConfiguration parseProperties(Properties props)
			throws ParserException {
		return in.parse(new PropertiesConfigurationParser(props),
				MainConfiguration.class);
	}
}
