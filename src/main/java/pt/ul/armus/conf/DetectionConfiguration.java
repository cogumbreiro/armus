package pt.ul.armus.conf;

/**
 * Configuration that corresponds to the deadlock detection algorithm.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class DetectionConfiguration implements Cloneable {
	/**
	 * Runs the deadlock detection algorithm?
	 */
	public boolean isEnabled;
	/**
	 * What is the detection delay?
	 */
	public long delay;
	/**
	 * What is the detection period?
	 */
	public long period;

	/**
	 * @param enabled
	 * @param delay
	 * @param period
	 */
	public DetectionConfiguration(boolean enabled, long delay, long period) {
		this.isEnabled = enabled;
		this.delay = delay;
		this.period = period;
	}

	@Override
	public String toString() {
		return "DetectionConfiguration [isEnabled=" + isEnabled + ", delay="
				+ delay + ", period=" + period + "]";
	}
	
	@Override
	public DetectionConfiguration clone() {
		return new DetectionConfiguration(isEnabled, delay, period);
	}
}
