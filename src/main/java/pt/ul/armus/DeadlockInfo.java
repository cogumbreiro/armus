package pt.ul.armus;

import pt.ul.armus.edgebuffer.TaskHandle;

public interface DeadlockInfo {
	boolean affects(TaskHandle handle);
	String toString();
}
