package pt.ul.armus;

import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ul.armus.cycledetector.CycleDetector;
import pt.ul.armus.edgebuffer.EdgeBuffer;
import pt.ul.armus.edgebuffer.EdgeSet;

class AutoGraphController implements StrategyController {
	private static final Logger LOG = Logger.getLogger(AutoGraphController.class
			.getName());
	private final EdgeBuffer buffer;
	private final CycleDetector detector;

	public AutoGraphController(EdgeBuffer buffer, CycleDetector detector) {
		this.buffer = buffer;
		this.detector = detector;
	}
	
	private Collection<Edge<Resource>> tryCollectEdges() {
		Collection<Edge<Resource>> store = new HashSet<Edge<Resource>>();
		int taskCount = 0;
		int edgeCount = 0;
		for (EdgeSet entry : buffer.getBlockedEdges()) {
			taskCount++;
			for (Edge<Resource> edge : entry) {
				if (store.add(edge)) {
					edgeCount++;
					if (edgeCount > (taskCount << 1)) {
						// if there are more resources than 2 times tasks, break
						return null;
					}
				}
			}
		}
		return store;
	}

	/**
	 * Checks for the existence of a deadlock.
	 * 
	 * @return null if there is no deadlock.
	 */
	public DeadlockInfo checkDeadlock() {
		Collection<Edge<Resource>> store = tryCollectEdges();
		return store != null ? doSG(store) : TaskGraphController.doWFG(buffer, detector);
	}
	
	private ResourceDeadlockInfo doSG(Collection<Edge<Resource>> store){
		int edges = store.size();
		LOG.log(Level.FINE, String.format("Detecting deadlocks  with SG, (size=%d)", edges));
		if (edges == 0) {
			return null;
		}
		Collection<Resource> result = detector.detect(store);
		if (result == null || result.isEmpty()) {
			return null;
		}
		return new ResourceDeadlockInfo(result);
	}

	@Override
	public EdgeBuffer getEdgeBuffer() {
		return buffer;
	}
}
