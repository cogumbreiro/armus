package pt.ul.armus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ul.armus.cycledetector.CycleDetector;
import pt.ul.armus.edgebuffer.EdgeBuffer;
import pt.ul.armus.edgebuffer.EdgeSet;
import pt.ul.armus.edgebuffer.TaskHandle;

class TaskResourceGraphController implements StrategyController {
	private static final Logger LOG = Logger.getLogger(TaskResourceGraphController.class
			.getName());
	private final EdgeBuffer buffer;
	private final CycleDetector detector;

	public TaskResourceGraphController(EdgeBuffer buffer, CycleDetector detector) {
		this.buffer = buffer;
		this.detector = detector;
	}

	/**
	 * Checks for the existence of a deadlock.
	 * 
	 * @return null if there is no deadlock.
	 */
	public DeadlockInfo checkDeadlock() {
		// create a snapshot of the edge sets
		Collection<TaskHandle> handles = new ArrayList<>(buffer.getHandles());
		Collection<Edge<Object>> store = new HashSet<Edge<Object>>();
		for (TaskHandle entry : handles) {
			// from allocated to task
			EdgeSet edges = entry.getBlocked();
			if (edges == null) {
				continue;
			}
			for (Resource alloc : edges.getAllocated()) {
				store.add(new Edge<>(entry, alloc));
			}
			// from task to requested
			for (Resource req : edges.getRequested()) {
				store.add(new Edge<>(req, entry));
			}
		}
		LOG.log(Level.FINE, String.format("Detecting deadlocks (size=%d)", store.size()));
		if (store.size() == 0) {
			return null;
		}
		Collection<Object> result = detector.detect(store);
		if (result == null || result.isEmpty()) {
			return null;
		}
		LOG.log(Level.FINE, String.format("DEADLOCK DETECTED (found=%d)", result.size()));
		return new TaskDeadlockInfo(result);
	}

	@Override
	public EdgeBuffer getEdgeBuffer() {
		return buffer;
	}
}
