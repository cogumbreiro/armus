package pt.ul.armus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ul.armus.cycledetector.CycleDetector;
import pt.ul.armus.edgebuffer.EdgeBuffer;
import pt.ul.armus.edgebuffer.EdgeSet;
import pt.ul.armus.edgebuffer.TaskHandle;

class TaskGraphController implements StrategyController {
	private static final Logger LOG = Logger
			.getLogger(TaskGraphController.class.getName());
	private final EdgeBuffer buffer;
	private final CycleDetector detector;

	public TaskGraphController(EdgeBuffer buffer, CycleDetector detector) {
		this.buffer = buffer;
		this.detector = detector;
	}

	@Override
	public DeadlockInfo checkDeadlock() {
		return doWFG(buffer, detector);
	}

	public static DeadlockInfo doWFG(EdgeBuffer buffer, CycleDetector detector) {
		// create a snapshot of the edge sets
		Collection<TaskHandle> handles = new ArrayList<>(buffer.getHandles());
		final int maxSize = handles.size();
		TaskHandle[] tids = new TaskHandle[maxSize];
		EdgeSet edges[] = new EdgeSet[maxSize];
		final int size;
		{
			int index = 0;
			for (TaskHandle task : handles) {
				tids[index] = task;
				edges[index] = task.getBlocked();
				if (edges[index] != null) {
					index++; // only record blocked tasks
				}
			}
			size = index; // overwrite the size to the number of blocked tasks
		}

		// for each resource, get the impeding tasks
		Map<Resource, List<Integer>> waitFor = new HashMap<>();
		for (int i = 0; i < size; i++) {
			EdgeSet resources = edges[i];
			for (Resource alloc : resources.getAllocated()) {
				List<Integer> tasks = waitFor.get(alloc);
				if (tasks == null) {
					tasks = new ArrayList<>();
					waitFor.put(alloc, tasks);
				}
				tasks.add(i);
			}
		}
		// now generate the wait-for-graph
		Collection<Edge<Integer>> store = new HashSet<>();
		for (int blockedTask = 0; blockedTask < size; blockedTask++) {
			for (Resource req : edges[blockedTask].getRequested()) {
				List<Integer> tmp = waitFor.get(req);
				if (tmp == null) {
					continue; // this task is blocked, but waits for no one
				}
				for (Integer culprit : tmp) {
					store.add(new Edge<Integer>(blockedTask, culprit));
				}
			}
		}
		int edgesCount = store.size();
		LOG.log(Level.FINE, String.format(
				"Detecting deadlocks  with WFG, (size=%d)", edgesCount));
		if (edgesCount == 0) {
			return null;
		}
		Collection<Integer> deadlock = detector.detect(store);
		if (deadlock == null || deadlock.isEmpty()) {
			return null;
		}
		List<TaskHandle> deadlockedTasks = new ArrayList<>();
		for (Integer taskId : deadlock) {
			deadlockedTasks.add(tids[taskId]);
		}
		return new TaskDeadlockInfo(deadlockedTasks);
	}

	@Override
	public EdgeBuffer getEdgeBuffer() {
		return buffer;
	}
}
