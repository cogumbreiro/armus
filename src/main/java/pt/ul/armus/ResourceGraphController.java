package pt.ul.armus;

import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ul.armus.cycledetector.CycleDetector;
import pt.ul.armus.edgebuffer.EdgeBuffer;
import pt.ul.armus.edgebuffer.EdgeSet;

class ResourceGraphController implements StrategyController {
	private static final Logger LOG = Logger.getLogger(ResourceGraphController.class
			.getName());
	private final EdgeBuffer buffer;
	private final CycleDetector detector;

	public ResourceGraphController(EdgeBuffer buffer, CycleDetector detector) {
		this.buffer = buffer;
		this.detector = detector;
	}

	/**
	 * Checks for the existence of a deadlock.
	 * 
	 * @return null if there is no deadlock.
	 */
	public DeadlockInfo checkDeadlock() {
		Collection<Edge<Resource>> store = new HashSet<Edge<Resource>>();
		for (EdgeSet entry : buffer.getBlockedEdges()) {
			for (Edge<Resource> edge : entry) {
				store.add(edge);
			}
		}
		int edges = store.size();
		LOG.log(Level.FINE, String.format("Detecting deadlocks (size=%d)", edges));
		if (edges == 0) {
			return null;
		}
		Collection<Resource> result = detector.detect(store);
		if (result == null || result.isEmpty()) {
			return null;
		}
		return new ResourceDeadlockInfo(result);
	}

	@Override
	public EdgeBuffer getEdgeBuffer() {
		return buffer;
	}
}
