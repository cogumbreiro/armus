package pt.ul.armus;

public class Edge<T> {
	private final T head;
	private final T tail;

	/**
	 * @param head
	 * @param tail
	 */
	public Edge(T head, T tail) {
		if (head == null || tail == null) {
			String msg = String.format(
					"Arguments cannot be null: (head=%s, tail=%s)", head, tail);
			throw new IllegalArgumentException(msg);
		}
		this.head = head;
		this.tail = tail;
	}

	public T getHead() {
		return head;
	}

	public T getTail() {
		return tail;
	}

	public String toString() {
		return "(" + head + ", " + tail + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + head.hashCode();
		result = prime * result + tail.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge<?> other = (Edge<?>) obj;
		return head.equals(other.head) && tail.equals(other.tail);
	}

}
