package pt.ul.armus;

import java.util.Collection;

import pt.ul.armus.edgebuffer.EdgeBuffer;


class ArmusImpl implements Armus {
	private final Collection<Component> plugins;
	private final StrategyController controller;

	public ArmusImpl(Collection<Component> plugins, StrategyController ctx) {
		this.plugins = plugins;
		this.controller = ctx;
	}

	@Override
	public void start() {
		for (Component plugin : plugins) {
			plugin.init(controller);
		}
		for (Component plugin : plugins) {
			plugin.onStart();
		}
	}

	@Override
	public void stop() {
		for (Component plugin : plugins) {
			plugin.onStop();
		}
		plugins.clear();
	}

	@Override
	public EdgeBuffer getBuffer() {
		return controller.getEdgeBuffer();
	}

	@Override
	public DeadlockInfo checkDeadlock() {
		return controller.checkDeadlock();
	}
}
