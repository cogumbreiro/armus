package pt.ul.armus;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ul.armus.deadlockresolver.DeadlockResolver;

/**
 * A deadlock detection algorithm.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
class DeadlockDetection implements Component {
	private static Logger LOG = Logger.getLogger(DeadlockDetection.class
			.getName());
	private final Timer timer = new Timer(true);
	private TimerTask monitor;
	private final DeadlockResolver deadlockResolver;
	/**
	 * What is the detection delay?
	 */
	public final long delay;
	/**
	 * What is the detection period?
	 */
	public final long period;

	/**
	 * Default constructor.
	 * @param deadlockResolver
	 * @param delay
	 * @param period
	 */
	public DeadlockDetection(DeadlockResolver deadlockResolver, long delay, long period) {
		this.deadlockResolver = deadlockResolver;
		this.delay = delay;
		this.period = period;
	}

	@Override
	public void onStart() {
		if (monitor == null) {
			throw new IllegalStateException("call init() first!");
		}
		LOG.log(Level.CONFIG, "verification strategy started");
		timer.schedule(monitor, delay, period);
	}

	@Override
	public void onStop() {
		LOG.log(Level.CONFIG, "stopping...");
		timer.cancel();
		LOG.log(Level.CONFIG, "stopped");
	}

	@Override
	public void init(final StrategyController controller) {
		this.monitor = new TimerTask() {
			@Override
			public void run() {
				DeadlockInfo deadlock = controller.checkDeadlock();
				if (deadlock != null) {
					LOG.log(Level.WARNING, "deadlock found " + deadlock);
					deadlockResolver.onDeadlockDetected(deadlock);
				}
			}
		};
	}
}
