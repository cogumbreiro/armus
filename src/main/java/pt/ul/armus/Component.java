package pt.ul.armus;

interface Component {
	void init(StrategyController ctx);
	void onStart();
	void onStop();
}
