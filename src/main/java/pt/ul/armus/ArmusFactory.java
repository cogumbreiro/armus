package pt.ul.armus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;

import pt.ul.armus.conf.MainConfiguration;
import pt.ul.armus.edgebuffer.EdgeBuffer;

/**
 * A Façade to current deadlock verification strategies.
 * 
 * @author Tiago Cogumbreiro
 * 
 * @param <R>
 */
public class ArmusFactory {
	private static final Logger LOG = Logger.getLogger(ArmusFactory.class.toString());
	public static Armus build(MainConfiguration conf) {
		if (conf.printConfig) {
			LOG.warning("Configuration loaded: " + conf);
		} else {
			LOG.config("Configuration loaded: " + conf);
		}
		Collection<Component> components = new ArrayList<>();
		if (conf.detection.isEnabled) {
			components.add(new DeadlockDetection(conf.deadlockResolver, conf.detection.delay, conf.detection.period));
		}
		if (conf.edgeBuffer.clearBufferAtTheEnd) {
			components.add(new ClearBuffer());
		}
		AvoidanceEdgeBuffer avoid = null;
		EdgeBuffer buffer = conf.edgeBuffer.factory.createEdgeBuffer();
		if (conf.avoidanceEnabled) {
			buffer = avoid = new AvoidanceEdgeBuffer(buffer);
		}
		final StrategyController ctrl;
		switch(conf.graphStrategy) {
		case AUTO:
			ctrl = new AutoGraphController(buffer, conf.cycleDetector); break;
		case SG:
			ctrl = new ResourceGraphController(buffer, conf.cycleDetector); break;
		case WFG:
			ctrl = new TaskGraphController(buffer, conf.cycleDetector); break;
		case TEG:
			ctrl = new TaskResourceGraphController(buffer, conf.cycleDetector); break;
		default:
			throw new IllegalArgumentException();
		}
		if (avoid != null) {
			avoid.setController(ctrl);
		}
		return new ArmusImpl(components, ctrl);
	}
}
