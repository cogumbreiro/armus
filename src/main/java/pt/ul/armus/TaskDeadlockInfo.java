package pt.ul.armus;

import java.util.Collection;

import pt.ul.armus.edgebuffer.TaskHandle;

public class TaskDeadlockInfo implements DeadlockInfo {
	public final Collection<?> deadlock;

	/**
	 * @param deadlock
	 */
	public TaskDeadlockInfo(Collection<?> deadlock) {
		this.deadlock = deadlock;
	}

	@Override
	public boolean affects(TaskHandle task) {
		return deadlock.contains(task);
	}
	
	@Override
	public String toString() {
		return deadlock.toString();
	}
}
