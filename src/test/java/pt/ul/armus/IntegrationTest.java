package pt.ul.armus;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pt.ul.armus.Armus;
import pt.ul.armus.ArmusFactory;
import pt.ul.armus.conf.DetectionConfiguration;
import pt.ul.armus.conf.EdgeBufferConfiguration;
import pt.ul.armus.conf.GraphStrategy;
import pt.ul.armus.conf.MainConfiguration;
import pt.ul.armus.cycledetector.CycleDetector;
import pt.ul.armus.cycledetector.jgraph.JGraphTSolver;
import pt.ul.armus.edgebuffer.EdgeBufferFactory;
import pt.ul.armus.edgebuffer.HashEdgeBufferFactory;
import pt.ul.armus.edgebuffer.QueueEdgeBufferFactory;

@RunWith(value = Parameterized.class)
public class IntegrationTest extends AbstractIntegrationTest {
	private final MainConfiguration conf;

	@Parameters(name = "{0}")
	public static Collection<Object[]> data() {
		ArrayList<Object[]> result = new ArrayList<>();
		JGraphTSolver cycleDetector = new JGraphTSolver();
		boolean[] trueFalse = { true, false };
		EdgeBufferFactory[] factories = { new HashEdgeBufferFactory(),
				new QueueEdgeBufferFactory() };
		for (GraphStrategy graph : GraphStrategy.values()) {
			for (boolean doAvoidance : trueFalse) {
				boolean doDetection = !doAvoidance;
				for (EdgeBufferFactory factory : factories) {
					EdgeBufferConfiguration eConf = new EdgeBufferConfiguration(
							true, factory);
					DetectionConfiguration detectConf = new DetectionConfiguration(
							doDetection, 0, 1);
					MainConfiguration conf = new MainConfiguration(detectConf,
							eConf, doAvoidance, cycleDetector, null, graph, false);
					result.add(new Object[] { conf });
				}
			}
		}
		return result;
	}

	public IntegrationTest(MainConfiguration conf) {
		conf = conf.clone();
		conf.deadlockResolver = this;
		this.conf = conf;
	}

	@Override
	protected Armus createStrategy(CycleDetector solver) {
		return ArmusFactory.build(conf);
	}
}
