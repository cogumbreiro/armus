package pt.ul.armus.cycledetector;

import pt.ul.armus.Resource;

public class BasicVariable implements Resource {

	private Object synch;

	public void setSynch(Object synch) {
		this.synch = synch;
	}

	@Override
	public Object getSynch() {
		return synch;
	}
}
