package pt.ul.armus.cycledetector;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import org.junit.Test;

import pt.ul.armus.Edge;
import pt.ul.armus.Resource;

public abstract class AbstractCycleDetectorTest {
	protected abstract CycleDetector createDetector();

	private static void assertSetEquals(Collection<?> col1, Collection<?> col2) {
		assertEquals(new HashSet<>(col1), new HashSet<>(col2));
	}
	
	@Test
	public void repeatedEdges() {
		CycleDetector solver = createDetector();
		Collection<Edge<Resource>> store = new HashSet<>();
		BasicVariable v1 = new BasicVariable();
		BasicVariable v2 = new BasicVariable();
		BasicVariable v3 = new BasicVariable();
		for (int i = 0; i < 100; i++) {
			store.add(new Edge<Resource>(v1, v1));
			store.add(new Edge<Resource>(v1, v2));
			store.add(new Edge<Resource>(v1, v3));
			store.add(new Edge<Resource>(v2, v1));
			store.add(new Edge<Resource>(v2, v2));
			store.add(new Edge<Resource>(v2, v3));
			store.add(new Edge<Resource>(v3, v1));
			store.add(new Edge<Resource>(v3, v2));
			store.add(new Edge<Resource>(v3, v3));
		}
		/*
		 * The cycle is: v1 -> v2 -> v1 Deadlocked vars: v1,
		 * v2 Deadlocked participants: P1, P3
		 */
		Collection<Resource> result = solver.detect(store);
		assertNotNull(result);
		assertSetEquals(asList(v1, v2, v3), result);
	}
	@Test
	public void looooongCycle() {
		CycleDetector solver = createDetector();
		Collection<Edge<Resource>> store = new HashSet<>();
		BasicVariable firstVar, var = new BasicVariable();
		firstVar = var;
		Collection<Resource> vars = new ArrayList<>();
		vars.add(firstVar);
		for (int i = 0; i < 1000; i++) {
			BasicVariable newVar = new BasicVariable();
			vars.add(newVar);
			store.add(new Edge<Resource>(var, newVar));
			var = newVar;
		}
		store.add(new Edge<Resource>(var, firstVar));
		
		/*
		 * The cycle is: v1 -> ... -> vn -> v1
		 * Deadlocked vars: v1, ..., vn
		 */
		Collection<Resource> result = solver.detect(store);
		assertNotNull(result);
		assertSetEquals(vars, result);
	}
	
	@Test
	public void minCyclePartsAndVars() {
		CycleDetector solver = createDetector();
		Collection<Edge<Resource>> store = new HashSet<>();
		BasicVariable v1 = new BasicVariable();
		BasicVariable v2 = new BasicVariable();
		BasicVariable v3 = new BasicVariable();
		BasicVariable v4 = new BasicVariable();
		BasicVariable v5 = new BasicVariable();
		store.add(new Edge<Resource>(v1, v2));
		store.add(new Edge<Resource>(v1, v3));
		store.add(new Edge<Resource>(v2, v1));
		store.add(new Edge<Resource>(v4, v5));
		/*
		 * The cycle is: v1 -> v2 -> v1 Deadlocked vars: v1,
		 * v2 Deadlocked participants: P1, P3
		 */
		Collection<Resource> result = solver.detect(store);
		assertNotNull(result);
		assertSetEquals(asList(v1, v2), result);
	}

	/**
	 * In this example we have an extra variable that should not be part of the
	 * cycle.
	 */
	@Test
	public void minCycleVars() {
		CycleDetector solver = createDetector();
		Collection<Edge<Resource>> store = new HashSet<>();
		BasicVariable v1 = new BasicVariable();
		BasicVariable v2 = new BasicVariable();
		BasicVariable v3 = new BasicVariable();
		store.add(new Edge<Resource>(v1, v2));
		store.add(new Edge<Resource>(v1, v3));
		store.add(new Edge<Resource>(v2, v1));
		/*
		 * The cycle is: v1 -> v2 -> v1 Deadlocked vars: v1,
		 */
		Collection<Resource> result = solver.detect(store);
		assertNotNull(result);
		assertSetEquals(asList(v1, v2), result);
	}

	/**
	 * In this example we want two cycles, each cycle is not connected to the
	 * other. There is still one variable that is not part of the cycle, but it
	 * is connected to the other variables.
	 */
	@Test
	public void findTwoCyclesTwoExtra() {
		CycleDetector solver = createDetector();
		Collection<Edge<Resource>> store = new HashSet<>();
		BasicVariable v1 = new BasicVariable();
		BasicVariable v2 = new BasicVariable();
		BasicVariable v3 = new BasicVariable();

		BasicVariable v4 = new BasicVariable();
		BasicVariable v5 = new BasicVariable();
		store.add(new Edge<Resource>(v1, v2));
		store.add(new Edge<Resource>(v1, v3));
		store.add(new Edge<Resource>(v2, v1));
		store.add(new Edge<Resource>(v4, v5));
		store.add(new Edge<Resource>(v5, v4));
		/*
		 * v3 is not part of the cycles
		 */
		Collection<Resource> result = solver.detect(store);
		assertNotNull(result);
		// the participants are
		assertSetEquals(asList(v1, v2, v4, v5), result);
	}
}
