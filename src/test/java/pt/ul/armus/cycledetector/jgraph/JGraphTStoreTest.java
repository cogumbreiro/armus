package pt.ul.armus.cycledetector.jgraph;

import pt.ul.armus.cycledetector.AbstractCycleDetectorTest;
import pt.ul.armus.cycledetector.CycleDetector;



public class JGraphTStoreTest extends AbstractCycleDetectorTest {
	@Override
	protected CycleDetector createDetector() {
		return new JGraphTSolver();
	}
}
