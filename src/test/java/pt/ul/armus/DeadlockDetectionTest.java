package pt.ul.armus;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.Test;

import pt.ul.armus.deadlockresolver.DeadlockResolver;

public class DeadlockDetectionTest {

	@Test
	public void onStartWithoutInitThrowsError() {
		DeadlockResolver res = mock(DeadlockResolver.class);
		DeadlockDetection det = new DeadlockDetection(res, 0, 100);
		try {
			det.onStart();
			fail();
		} catch (IllegalStateException e) {
			// ok
		}
	}

	@Test
	public void noDeadlocks() {
		DeadlockResolver res = mock(DeadlockResolver.class);
		DeadlockDetection det = new DeadlockDetection(res, 0, 100);
		StrategyController ctrl = mock(StrategyController.class);
		when(ctrl.checkDeadlock()).thenReturn(null);
		det.init(ctrl);
		det.onStart();
		// check that deadlock detection is called
		verify(ctrl, timeout(200).atLeast(1)).checkDeadlock();
		verifyNoMoreInteractions(res);
		det.onStop();
	}

	@Test
	public void deadlock() throws InterruptedException {
		DeadlockResolver res = mock(DeadlockResolver.class);
		DeadlockDetection det = new DeadlockDetection(res, 0, 100);
		StrategyController ctrl = mock(StrategyController.class);
		DeadlockInfo deadlock = mock(DeadlockInfo.class);
		when(ctrl.checkDeadlock()).thenReturn(deadlock);
		det.init(ctrl);
		det.onStart();
		// check that deadlock detection is called
		verify(ctrl, timeout(200).atLeast(1)).checkDeadlock();
		verify(res, timeout(200).atLeast(1)).onDeadlockDetected(deadlock);
		det.onStop();
	}

}
