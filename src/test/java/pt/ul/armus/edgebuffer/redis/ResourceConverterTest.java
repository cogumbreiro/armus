package pt.ul.armus.edgebuffer.redis;

import static org.junit.Assert.*;

import org.junit.Test;

import pt.ul.armus.edgebuffer.redis.RedisResource;
import pt.ul.armus.edgebuffer.redis.ResourceConverter;

public class ResourceConverterTest {

	@Test
	public void equalsWorks() {
		RedisResource res1 = new RedisResource("foo");
		RedisResource res2 = new RedisResource("foo");
		assertEquals(res1, res2);
	}

	@Test
	public void differentWorks() {
		RedisResource res1 = new RedisResource("foo");
		RedisResource res2 = new RedisResource("bar");
		assertFalse(res1.equals(res2));
	}
	
	@Test
	public void simple() {
		RedisResource res = new RedisResource("foo");
		ResourceConverter conv = new ResourceConverter();
		assertEquals(res, conv.fromData(conv.toData(res)));
	}
}
