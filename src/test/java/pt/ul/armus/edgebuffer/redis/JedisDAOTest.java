package pt.ul.armus.edgebuffer.redis;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pt.ul.armus.Resource;
import pt.ul.armus.edgebuffer.EdgeSet;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class JedisDAOTest {
	static class MyPool extends JedisPool {

		public MyPool(GenericObjectPoolConfig poolConfig, String host,
				int port, int timeout, String password, int database,
				String clientName) {
			super(poolConfig, host, port, timeout, password, database, clientName);
		}

		public MyPool(GenericObjectPoolConfig poolConfig, String host,
				int port, int timeout, String password, int database) {
			super(poolConfig, host, port, timeout, password, database);
		}

		public MyPool(GenericObjectPoolConfig poolConfig, String host,
				int port, int timeout, String password) {
			super(poolConfig, host, port, timeout, password);
		}

		public MyPool(GenericObjectPoolConfig poolConfig, String host,
				int port, int timeout) {
			super(poolConfig, host, port, timeout);
		}

		public MyPool(GenericObjectPoolConfig poolConfig, String host, int port) {
			super(poolConfig, host, port);
		}

		public MyPool(GenericObjectPoolConfig poolConfig, String host) {
			super(poolConfig, host);
		}

		public MyPool(String host, int port) {
			super(host, port);
		}

		public MyPool(String arg0) {
			super(arg0);
		}

		public MyPool(URI uri) {
			super(uri);
		}
		
		public GenericObjectPool<Jedis> getInternalPool() {
			return this.internalPool;
		}
	}
	
	Converter<Resource, String> conv;
	MyPool jedisPool = new MyPool("localhost");
	Pool pool = new Pool(jedisPool);
	KeyGenerator keyGen = new KeyGenerator("armus");
	final String taskKey = keyGen.getTaskId(Long.valueOf(1));
	final String tasks = keyGen.getTasks();
	final String pred = keyGen.getPredecessors(taskKey);
	final String succ = keyGen.getSuccessors(taskKey);
	JedisDAO dao;
	
	@Before
	@SuppressWarnings("unchecked")
	public void setUp() {
		conv = mock(Converter.class);
		dao = new JedisDAO(jedisPool, conv, keyGen);
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			j.flushAll();
		}
		assertNoLeaks();
	}
	
	@After
	public void tearDown() {
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			j.flushAll();
		}
	}
	
	public void assertNoLeaks() {
		assertEquals(1, jedisPool.getInternalPool().getCreatedCount());
		assertEquals(0, jedisPool.getInternalPool().getNumActive());
	}

	@Test
	public void setBlocked() {
		EdgeSet s = mock(EdgeSet.class);
		Resource f = mock(Resource.class);
		Resource g = mock(Resource.class);
		Resource h = mock(Resource.class);
		when(s.getAllocated()).thenReturn(Arrays.asList(f));
		when(s.getRequested()).thenReturn(Arrays.asList(g, h));
		when(conv.toData(f)).thenReturn("f");
		when(conv.toData(g)).thenReturn("g");
		when(conv.toData(h)).thenReturn("h");
		
		// run
		dao.setBlocked(taskKey, s);
		
		// checks
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			assertEquals(Long.valueOf(1), j.llen(pred));
			assertEquals(Long.valueOf(2), j.llen(succ));
			assertEquals("f", j.lpop(pred));
			assertEquals("h", j.lpop(succ));
			assertEquals("g", j.lpop(succ));
		}
	}
	
	private void addBlocked() {
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			j.incr(keyGen.getLastTaskId());
			j.sadd(tasks, taskKey);
			j.lpush(pred, "a");
			j.lpush(succ, "b");
			j.lpush(succ, "c");
		}
	}
	
	private void addUnblockedPred() {
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			j.incr(keyGen.getLastTaskId());
			j.sadd(tasks, taskKey);
			j.lpush(succ, "b");
			j.lpush(succ, "c");
		}
	}
	private void addUnblockedSucc() {
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			j.incr(keyGen.getLastTaskId());
			j.sadd(tasks, taskKey);
			j.lpush(pred, "a");
		}
	}
	private void addUnknownTask() {
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			j.lpush(pred, "a");
			j.lpush(succ, "b");
			j.lpush(succ, "c");
		}
	}

	@Test
	public void isBlocked() {
		addBlocked();
		assertTrue(dao.isBlocked(taskKey));
	}

	@Test
	public void isUnblockedSetPred() {
		addUnblockedPred();
		assertFalse(dao.isBlocked(taskKey));
	}

	@Test
	public void isUnblockedSetSucc() {
		addUnblockedSucc();
		assertFalse(dao.isBlocked(taskKey));
	}

	@Test
	public void isUnblockedByDefault() {
		addUnblockedPred();
		assertFalse(dao.isBlocked(taskKey));
	}

	@Test
	public void clearBlocked() {
		// blocked
		addBlocked();
		dao.clearBlocked(taskKey);
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			// the keys must be gone
			assertFalse(j.exists(pred));
			assertFalse(j.exists(succ));
		}
	}

	@Test
	public void removeBlockedTask() {
		addBlocked();
		dao.removeTask(taskKey);
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			// the keys must be gone
			assertFalse(j.sismember(tasks, taskKey));
			assertFalse(j.exists(pred));
			assertFalse(j.exists(succ));
		}
	}
	
	@Test
	public void removeUnblockedTask1() {
		addUnblockedPred();
		dao.removeTask(taskKey);
		assertNoLeaks();
		
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			// the keys must be gone
			assertFalse(j.sismember(tasks, taskKey));
			assertFalse(j.exists(pred));
			assertFalse(j.exists(succ));
		}
	}

	@Test
	public void removeUnblockedTask2() {
		addUnblockedSucc();
		dao.removeTask(taskKey);
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			// the keys must be gone
			assertFalse(j.sismember(tasks, taskKey));
			assertFalse(j.exists(pred));
			assertFalse(j.exists(succ));
		}
	}
	
	@Test
	public void removeUnblockedTask3() {
		addUnknownTask();
		dao.removeTask(taskKey);
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			// the keys must be gone
			assertFalse(j.sismember(tasks, taskKey));
			assertFalse(j.exists(pred));
			assertFalse(j.exists(succ));
		}
	}
	
	@Test
	public void removeAllEmpty() {
		dao.removeAll();
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			// the keys must be gone
			assertFalse(j.exists(tasks));
			assertFalse(j.exists(keyGen.getLastTaskId()));
		}
	}
	
	@Test
	public void removeAllUnblockedPred() {
		addUnblockedPred();
		dao.removeAll();
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			// the keys must be gone
			assertFalse(j.exists(tasks));
			assertFalse(j.exists(keyGen.getLastTaskId()));
		}
	}

	@Test
	public void removeAllUnblockedSucc() {
		addUnblockedSucc();
		dao.removeAll();
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			// the keys must be gone
			assertFalse(j.exists(tasks));
			assertFalse(j.exists(keyGen.getLastTaskId()));
		}
	}

	@Test
	public void testCreateTask() {
		String task = dao.createTask();
		assertNoLeaks();
		try (PoolEntry entry = pool.getResource()) {
			Jedis j = entry.getJedis();
			assertTrue(j.sismember(tasks, task));
			assertEquals("1", j.get(keyGen.getLastTaskId()));
		}
	}

	@Test
	public void getAllOne() {
		addBlocked();
		Resource a = mock(Resource.class);
		Resource b = mock(Resource.class);
		Resource c = mock(Resource.class);
		when(conv.fromData("a")).thenReturn(a);
		when(conv.fromData("b")).thenReturn(b);
		when(conv.fromData("c")).thenReturn(c);
		Iterator<EdgeSet> all = dao.getAllBlocked().iterator();
		assertNoLeaks();
		assertTrue(all.hasNext());
		EdgeSet s = all.next();
		assertFalse(all.hasNext());
		Iterator<Resource> predecessors = s.getAllocated().iterator();
		assertTrue(predecessors.hasNext());
		assertEquals(a, predecessors.next());
		assertFalse(predecessors.hasNext());
		
		Iterator<Resource> successors = s.getRequested().iterator();
		assertEquals(c, successors.next());
		assertEquals(b, successors.next());
	}

	@Test
	public void getAllStartsEmpty() {
		assertFalse(dao.getAllBlocked().iterator().hasNext());
	}
}
