package pt.ul.armus.edgebuffer;

public class QueueEdgeBufferTest extends AbstractEdgeBufferTest {

	@Override
	protected EdgeBuffer createEdgeBuffer() {
		return new QueueEdgeBuffer(factory);
	}
}
