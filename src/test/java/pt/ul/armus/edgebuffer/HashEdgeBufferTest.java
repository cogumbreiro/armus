package pt.ul.armus.edgebuffer;

public class HashEdgeBufferTest extends AbstractEdgeBufferTest {

	@Override
	protected EdgeBuffer createEdgeBuffer() {
		return new HashEdgeBuffer(factory);
	}
}
