package pt.ul.armus.edgebuffer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

public abstract class AbstractEdgeBufferTest {
	private static Collection<?> toSet(Iterable<?> iter) {
		HashSet<Object> result = new HashSet<>();
		for (Object elem : iter) {
			result.add(elem);
		}
		return result;
	}

	private static void assertEqualsUnordered(Iterable<?> expected,
			Iterable<?> obtained) {
		assertEquals(toSet(expected), toSet(obtained));
	}

	private void assertBufferContains(EdgeSet... edges) {
		assertEqualsUnordered(Arrays.asList(edges), buffer.getBlockedEdges());
	}

	protected abstract EdgeBuffer createEdgeBuffer();

	TaskHandleFactory factory;
	EdgeBuffer buffer;

	@Before
	public void setUp() {
		factory = mock(TaskHandleFactory.class);
		buffer = createEdgeBuffer();
	}

	@Test
	public void startsEmpty() {
		assertBufferContains();
	}

	@Test
	public void clearEmptyMustWork() {
		buffer.clear();
		assertBufferContains();
	}

	@Test
	public void addOne() {
		TaskHandle handle = mock(TaskHandle.class);
		when(factory.createTaskHandle()).thenReturn(handle);
		assertSame(handle, buffer.createHandle());
	}

	@Test
	public void addUnblockedThenIterEmpty() {
		TaskHandle handle = mock(TaskHandle.class);
		when(factory.createTaskHandle()).thenReturn(handle);
		assertBufferContains();
	}

	@Test
	public void addBlockedThenIterOnce() {
		TaskHandle handle = mock(TaskHandle.class);
		when(factory.createTaskHandle()).thenReturn(handle);
		EdgeSet set1 = mock(EdgeSet.class);
		when(handle.getBlocked()).thenReturn(set1);
		buffer.createHandle();
		buffer.createHandle();
		assertBufferContains(set1);
	}

	@Test
	public void addTwoBlocked() {
		TaskHandle handle1 = mock(TaskHandle.class);
		TaskHandle handle2 = mock(TaskHandle.class);
		when(factory.createTaskHandle()).thenReturn(handle1)
				.thenReturn(handle2);
		assertSame(handle1, buffer.createHandle());
		assertSame(handle2, buffer.createHandle());
		EdgeSet set1 = mock(EdgeSet.class);
		EdgeSet set2 = mock(EdgeSet.class);
		when(handle1.getBlocked()).thenReturn(set1);
		when(handle2.getBlocked()).thenReturn(set2);
		assertBufferContains(set1, set2);
	}

	@Test
	public void addTwoThenClear() {
		TaskHandle handle1 = mock(TaskHandle.class);
		TaskHandle handle2 = mock(TaskHandle.class);
		when(factory.createTaskHandle()).thenReturn(handle1)
				.thenReturn(handle2);
		buffer.clear();
		assertBufferContains();
	}

	@Test
	public void addTwoBlockedRemoveOne1() {
		TaskHandle handle1 = mock(TaskHandle.class);
		TaskHandle handle2 = mock(TaskHandle.class);
		when(factory.createTaskHandle()).thenReturn(handle1)
				.thenReturn(handle2);
		assertSame(handle1, buffer.createHandle());
		assertSame(handle2, buffer.createHandle());
		EdgeSet set1 = mock(EdgeSet.class);
		EdgeSet set2 = mock(EdgeSet.class);
		when(handle1.getBlocked()).thenReturn(set1);
		when(handle2.getBlocked()).thenReturn(set2);
		buffer.destroy(handle1);
		assertBufferContains(set2);
	}

	@Test
	public void addTwoBlockedRemoveOne2() {
		TaskHandle handle1 = mock(TaskHandle.class);
		TaskHandle handle2 = mock(TaskHandle.class);
		when(factory.createTaskHandle()).thenReturn(handle1)
				.thenReturn(handle2);
		assertSame(handle1, buffer.createHandle());
		assertSame(handle2, buffer.createHandle());
		EdgeSet set1 = mock(EdgeSet.class);
		EdgeSet set2 = mock(EdgeSet.class);
		when(handle1.getBlocked()).thenReturn(set1);
		when(handle2.getBlocked()).thenReturn(set2);
		buffer.destroy(handle2);
		assertBufferContains(set1);
	}

	@Test
	public void addTwoBlockedRemoveBoth() {
		TaskHandle handle1 = mock(TaskHandle.class);
		TaskHandle handle2 = mock(TaskHandle.class);
		when(factory.createTaskHandle()).thenReturn(handle1)
				.thenReturn(handle2);
		assertSame(handle1, buffer.createHandle());
		assertSame(handle2, buffer.createHandle());
		EdgeSet set1 = mock(EdgeSet.class);
		EdgeSet set2 = mock(EdgeSet.class);
		when(handle1.getBlocked()).thenReturn(set1);
		when(handle2.getBlocked()).thenReturn(set2);
		buffer.destroy(handle1);
		buffer.destroy(handle2);
		assertBufferContains();
	}
}
