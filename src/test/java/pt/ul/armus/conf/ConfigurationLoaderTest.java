package pt.ul.armus.conf;

import static org.junit.Assert.assertEquals;

import java.util.Properties;

import org.junit.Test;

import pt.ul.armus.conf.in.ParserException;

public class ConfigurationLoaderTest {
	
	@Test
	public void prefix() {
		Properties props = ConfigurationLoader.getSystemProperties("foo", ConfigurationLoader.getDefault());
		assertEquals(props, ConfigurationLoader.renderNoPrefix(ConfigurationLoader.DEFAULT));
	}
	
	@Test
	public void step1() {
		Properties props = ConfigurationLoader.getSystemProperties("lol");
		assertEquals(props, ConfigurationLoader.renderNoPrefix(ConfigurationLoader.DEFAULT));
	}
	
	@Test
	public void inOut() throws ParserException {
		Properties props = ConfigurationLoader.getSystemProperties("lol");
		MainConfiguration conf = ConfigurationLoader.parseProperties(props);
		assertEquals(props, ConfigurationLoader.renderNoPrefix(conf));
	}

}
