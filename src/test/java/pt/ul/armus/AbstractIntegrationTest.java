package pt.ul.armus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

import java.util.Collection;
import java.util.HashSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import pt.ul.armus.cycledetector.CycleDetector;
import pt.ul.armus.cycledetector.jgraph.JGraphTSolver;
import pt.ul.armus.deadlockresolver.DeadlockFoundException;
import pt.ul.armus.deadlockresolver.DeadlockResolver;
import pt.ul.armus.edgebuffer.EdgeBuffer;
import pt.ul.armus.edgebuffer.EdgeSet;
import pt.ul.armus.edgebuffer.EdgeSetFactory;
import pt.ul.armus.edgebuffer.TaskHandle;

@Ignore
public abstract class AbstractIntegrationTest implements DeadlockResolver {
	protected Armus av;
	protected volatile DeadlockInfo deadlock;
	protected long sleepPeriodMillis = 100;

	protected abstract Armus createStrategy(CycleDetector solver);
	
	@Before
	public void setUp() {
		deadlock = null;
		CycleDetector solver = new JGraphTSolver();
		av = createStrategy(solver);
		av.start();
	}

	@Override
	public synchronized void onDeadlockDetected(DeadlockInfo d) {
		deadlock = d;
		notifyAll();
	}

	@After
	public void tearDown() {
		av.stop();
	}

	public static void assertCollectionEquals(Collection<?> expected,
			Collection<?> obtained) {
		if (expected == null || obtained == null) {
			assertEquals(expected, obtained);
		} else {
			assertEquals(new HashSet<>(expected), new HashSet<>(obtained));
		}
	}

	public void assertNoDeadlock() {
		try {
			Thread.sleep(sleepPeriodMillis);
			assertNull(deadlock); // no deadlock found
		} catch (InterruptedException e) {
			fail(e.toString());
		}
	}
/*
	public void assertHasDeadlock(Resource... expected) {
		assertHasDeadlock(asList(expected));
	}
	
	public void assertHasDeadlock(Collection<Resource> expected) {
		synchronized(this) {
			try {
				long elapsed = System.currentTimeMillis() + sleepPeriodMillis;
				while (elapsed >= System.currentTimeMillis() && deadlock == null) {
					wait(sleepPeriodMillis);
				}
			} catch (InterruptedException e) {
				fail(e.toString());
			}
		}
		assertCollectionEquals(expected, deadlock);
	}
*/
	public void assertDeadlocked(TaskHandle handle) {
		synchronized(this) {
			try {
				long elapsed = System.currentTimeMillis() + sleepPeriodMillis;
				while (elapsed >= System.currentTimeMillis() && deadlock == null) {
					wait(sleepPeriodMillis);
				}
			} catch (InterruptedException e) {
				fail(e.toString());
			}
		}
		assertNotNull("Deadlock not detected!", deadlock);
		assertTrue(deadlock.affects(handle));
	}
	
	@Test
	public void addEdge() throws DeadlockFoundException {
		TaskHandle h = av.getBuffer().createHandle();
		Resource left = mock(Resource.class);
		Resource right = mock(Resource.class);
		EdgeSet leftToRight = EdgeSetFactory.requestOneAllocateOne(left, right);
		h.setBlocked(leftToRight);
		assertNoDeadlock();
	}

	@Test
	public void handleLifeCycle2() throws DeadlockFoundException {
		EdgeBuffer buffer = av.getBuffer();
		TaskHandle h = buffer.createHandle();
		Resource left = mock(Resource.class);
		Resource right = mock(Resource.class);
		EdgeSet leftToRight = EdgeSetFactory.requestOneAllocateOne(left, right);
		h.setBlocked(leftToRight);
		try {
			buffer.destroy(h);
			fail();
		} catch (IllegalStateException e) {
			// ok
		}
	}

	@Test
	public void handleLifeCycle3() throws DeadlockFoundException {
		TaskHandle h = av.getBuffer().createHandle();
		EdgeBuffer buffer = av.getBuffer();
		for (int i = 0; i < 100; i++) {
			Resource left = mock(Resource.class);
			Resource right = mock(Resource.class);
			EdgeSet leftToRight = EdgeSetFactory.requestOneAllocateOne(left, right);
			h.setBlocked(leftToRight);
			h.clearBlocked();
		}
		buffer.destroy(h);
	}

	@Test
	public void handleLifeCycle4() throws DeadlockFoundException {
		TaskHandle h = av.getBuffer().createHandle();
		Resource left = mock(Resource.class);
		Resource right = mock(Resource.class);
		EdgeSet leftToRight = EdgeSetFactory.requestOneAllocateOne(left, right);
		h.setBlocked(leftToRight);
		try {
			h.setBlocked(leftToRight);
			fail("Should have thrown IllegalStateException(), as setBlocked cannot be called twice.");
		} catch (IllegalStateException e) {
			// ok
		}
	}

	@Test
	public void handleLifeCycle5() throws DeadlockFoundException {
		TaskHandle h = av.getBuffer().createHandle();
		//MockResource left = new MockResource();
		//MockResource right = new MockResource();
		//EdgeSet leftToRight = new RHSGroup(left,
		//		asList((Resource) right));
		try {
			h.clearBlocked();
			fail("Should have thrown IllegalStateException(), as it is already cleared.");
		} catch (IllegalStateException e) {
			// ok
		}
	}

	@Test
	public void handleLifeCycle1() throws DeadlockFoundException {
		TaskHandle h = av.getBuffer().createHandle();
		av.getBuffer().destroy(h);
	}

	@Test
	public void addEdgeAddEdge() throws DeadlockFoundException {
		Resource left1 = mock(Resource.class);
		Resource right1 = mock(Resource.class);
		Resource left2 = mock(Resource.class);
		Resource right2 = mock(Resource.class);
		EdgeSet leftToRight1 = EdgeSetFactory.requestOneAllocateOne(left1, right1);
		EdgeSet leftToRight2 = EdgeSetFactory.requestOneAllocateOne(left2, right2);
		EdgeBuffer buffer = av.getBuffer();
		TaskHandle t1 = buffer.createHandle();
		t1.setBlocked(leftToRight1);
		TaskHandle t2 = buffer.createHandle();
		t2.setBlocked(leftToRight2);
		assertNoDeadlock();
	}

	@Test
	public void addEdgeRemove() throws DeadlockFoundException {
		for (int i = 0; i < 100; i++) {
			Resource left = mock(Resource.class);
			Resource right = mock(Resource.class);
			EdgeSet leftToRight = EdgeSetFactory.requestOneAllocateOne(left, right);
			TaskHandle h = av.getBuffer().createHandle();
			h.setBlocked(leftToRight);
			h.clearBlocked();
		}
		assertNoDeadlock();
	}

	@Test
	public void noDeadlock() {
		for (int i = 0; i < 100; i++) {
			TaskHandle h = av.getBuffer().createHandle();
			Resource left = mock(Resource.class);
			Resource right = mock(Resource.class);
			EdgeSet deadlockSelf = EdgeSetFactory.requestOneAllocateOne(left, right);
			try {
				h.setBlocked(deadlockSelf);
			} catch (DeadlockFoundException e) {
				fail();
			}
		}
		assertNoDeadlock();
	}

	@Test
	public void deadlockSelf() {
		TaskHandle h = av.getBuffer().createHandle();
		Resource res = mock(Resource.class);
		EdgeSet deadlockSelf = EdgeSetFactory.requestOneAllocateOne(res, res);
		try {
			h.setBlocked(deadlockSelf);
			assertDeadlocked(h);
		} catch (DeadlockFoundException e) {
			// ok
		}
	}

	/*
	 * When another task creates a deadlock, it should not influence other
	 * tasks.
	 */
	@Test
	public void deadlockByOther() {
		TaskHandle h = av.getBuffer().createHandle();
		Resource left = mock(Resource.class);
		EdgeSet deadlockSelf = EdgeSetFactory.requestOneAllocateOne(left, left);
		try {
			h.setBlocked(deadlockSelf);
		} catch (DeadlockFoundException e) {
			TaskHandle t2 = av.getBuffer().createHandle();
			Resource l2 = mock(Resource.class);
			Resource r2 = mock(Resource.class);
			EdgeSet edge = EdgeSetFactory.requestOneAllocateOne(l2, r2);
			try {
				t2.setBlocked(edge);
				t2.clearBlocked();
				av.getBuffer().destroy(t2);
			} catch (DeadlockFoundException e1) {
				fail();
			}
		}
	}
}
