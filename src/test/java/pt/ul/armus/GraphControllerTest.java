package pt.ul.armus;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import pt.ul.armus.cycledetector.jgraph.JGraphTSolver;
import pt.ul.armus.deadlockresolver.DeadlockFoundException;
import pt.ul.armus.edgebuffer.EdgeSetFactory;
import pt.ul.armus.edgebuffer.HashEdgeBufferFactory;
import pt.ul.armus.edgebuffer.TaskHandle;

public class GraphControllerTest {
	HashEdgeBufferFactory ebFactory;
	JGraphTSolver cycleDetector;
	TaskGraphController ctrl;

	@Before
	public void setUp() {
		ebFactory = new HashEdgeBufferFactory();
		cycleDetector = new JGraphTSolver();
		ctrl = new TaskGraphController(ebFactory.createEdgeBuffer(), cycleDetector);
	}
	
	@Test
	public void noDeadlock() {
		assertNull(ctrl.checkDeadlock());
	}
	
	@Test
	public void OneTaskNoDeadlock() {
		ctrl.getEdgeBuffer().createHandle();
		assertNull(ctrl.checkDeadlock());
	}
	
	@Test
	public void OneTaskSelfDeadlock1() throws DeadlockFoundException {
		TaskHandle task = ctrl.getEdgeBuffer().createHandle();
		Resource res = mock(Resource.class);
		task.setBlocked(EdgeSetFactory.requestOneAllocateOne(res, res));
		DeadlockInfo nfo = ctrl.checkDeadlock();
		assertNotNull(nfo);
		assertTrue(nfo.affects(task));
	}

	@Test
	public void OneTaskSelfDeadlock2() throws DeadlockFoundException {
		TaskHandle task = ctrl.getEdgeBuffer().createHandle();
		Resource res1 = mock(Resource.class);
		Resource res2 = mock(Resource.class);
		Resource res3 = mock(Resource.class);
		task.setBlocked(EdgeSetFactory.requestManyAllocateMany(Arrays.asList(res3, res1), Arrays.asList(res2, res1)));
		DeadlockInfo nfo = ctrl.checkDeadlock();
		assertNotNull(nfo);
		assertTrue(nfo.affects(task));
	}
	
	@Test
	public void OneTaskSelfDeadlock3() throws DeadlockFoundException {
		TaskHandle t1 = ctrl.getEdgeBuffer().createHandle();
		TaskHandle t2 = ctrl.getEdgeBuffer().createHandle();
		Resource res1 = mock(Resource.class);
		Resource res2 = mock(Resource.class);
		Resource res3 = mock(Resource.class);
		t1.setBlocked(EdgeSetFactory.requestManyAllocateMany(Arrays.asList(res3, res1), Arrays.asList(res2, res1)));
		t2.setBlocked(EdgeSetFactory.requestManyAllocateMany(Arrays.asList(res3), Arrays.asList(res2)));
		DeadlockInfo nfo = ctrl.checkDeadlock();
		assertNotNull(nfo);
		assertTrue(nfo.affects(t1));
		assertFalse(nfo.affects(t2));
	}
	
	@Test
	public void TwoTasksDeadlock1() throws DeadlockFoundException {
		TaskHandle t1 = ctrl.getEdgeBuffer().createHandle();
		TaskHandle t2 = ctrl.getEdgeBuffer().createHandle();
		Resource r1 = mock(Resource.class);
		Resource r2 = mock(Resource.class);
		t1.setBlocked(EdgeSetFactory.requestOneAllocateOne(r1, r2));
		t2.setBlocked(EdgeSetFactory.requestOneAllocateOne(r2, r1));
		DeadlockInfo nfo = ctrl.checkDeadlock();
		assertNotNull(nfo);
		assertTrue(nfo.affects(t1));
		assertTrue(nfo.affects(t2));
	}

	@Test
	public void ThreeTasksDeadlock1() throws DeadlockFoundException {
		TaskHandle t1 = ctrl.getEdgeBuffer().createHandle();
		TaskHandle t2 = ctrl.getEdgeBuffer().createHandle();
		TaskHandle t3 = ctrl.getEdgeBuffer().createHandle();
		Resource r1 = mock(Resource.class);
		Resource r2 = mock(Resource.class);
		Resource r3 = mock(Resource.class);
		t1.setBlocked(EdgeSetFactory.requestOneAllocateOne(r1, r2));
		t2.setBlocked(EdgeSetFactory.requestOneAllocateOne(r2, r3));
		t3.setBlocked(EdgeSetFactory.requestOneAllocateOne(r3, r1));
		DeadlockInfo nfo = ctrl.checkDeadlock();
		assertNotNull(nfo);
		assertTrue(nfo.affects(t1));
		assertTrue(nfo.affects(t2));
		assertTrue(nfo.affects(t3));
	}
}
