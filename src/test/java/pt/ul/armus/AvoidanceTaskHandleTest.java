package pt.ul.armus;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pt.ul.armus.deadlockresolver.DeadlockFoundException;
import pt.ul.armus.edgebuffer.EdgeSet;
import pt.ul.armus.edgebuffer.TaskHandle;

public class AvoidanceTaskHandleTest {

	TaskHandle delegate;
	AvoidanceTaskHandle handle;
	StrategyController ctl;
	
	@Before
	public void setUp() {
		delegate = mock(TaskHandle.class);
		ctl = mock(StrategyController.class);
		handle = new AvoidanceTaskHandle(delegate, ctl);
	}
	
	@After
	public void tearDown() {
		verifyNoMoreInteractions(delegate, ctl);
	}
	
	@Test
	public void setBlockedNoDeadlock() throws DeadlockFoundException {
		EdgeSet newEdges = mock(EdgeSet.class);
		when(ctl.checkDeadlock()).thenReturn(null);
		handle.setBlocked(newEdges);
		verify(ctl).checkDeadlock();
		verify(delegate).setBlocked(newEdges);
	}
	
	@Test
	public void deadlockAffectsTask() throws DeadlockFoundException {
		EdgeSet newEdges = mock(EdgeSet.class);
		DeadlockInfo deadlock = mock(DeadlockInfo.class);
		when(ctl.checkDeadlock()).thenReturn(deadlock);
		when(deadlock.affects(handle)).thenReturn(true);
		try {
			handle.setBlocked(newEdges);
			fail();
		} catch (DeadlockFoundException e){
			// ok
		}
		verify(ctl).checkDeadlock();
		verify(delegate).setBlocked(newEdges);
	}
	
	@Test
	public void deadlockNotAffectsTask() throws DeadlockFoundException {
		EdgeSet newEdges = mock(EdgeSet.class);
		DeadlockInfo deadlock = mock(DeadlockInfo.class);
		when(ctl.checkDeadlock()).thenReturn(deadlock);
		when(deadlock.affects(delegate)).thenReturn(false);
		try {
			handle.setBlocked(newEdges);
		} catch (DeadlockFoundException e){
			fail();
		}
		verify(ctl).checkDeadlock();
		verify(delegate).setBlocked(newEdges);
	}
	
	@Test
	public void isBlocked() {
		when(delegate.isBlocked()).thenReturn(true);
		assertTrue(handle.isBlocked());
		verify(delegate).isBlocked();
	}
	
	@Test
	public void isUnblocked() {
		when(delegate.isBlocked()).thenReturn(false);
		assertFalse(handle.isBlocked());
		verify(delegate).isBlocked();
	}

	@Test
	public void testGetBlocked() {
		EdgeSet edgeSet = mock(EdgeSet.class);
		when(delegate.getBlocked()).thenReturn(edgeSet);
		assertSame(edgeSet, handle.getBlocked());
		verify(delegate).getBlocked();
	}

	@Test
	public void testClearBlocked() {
		delegate.clearBlocked();
		verify(delegate).clearBlocked();
	}

}
